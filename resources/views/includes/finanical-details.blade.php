
<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
  <h6> Finanical Details</h6>
</div>
<form class="form-horizontal update-account-form" id="update-distributor-communication-form" action="javascript:void(0)">
    <div class="form-group row">
      <label for="inputEmail" class="col-sm-4 col-form-label">Monthly Steel Req</label>
      <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Monthly Steel Requirement" id="monthly_steel_req" name="monthly_steel_req" value="">
      </div>
    </div>

    <div class="form-group row">
      <label for="inputEmail" class="col-sm-4 col-form-label">Annual Turnover </label>
      <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Enter Annual Turnover" id="annual_turnover" name="annual_turnover" value="">
      </div>
    </div>


    <div class="form-group row">
      <label for="inputEmail" class="col-sm-4 col-form-label">Bank Address</label>
      <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Enter Bank Address" id="bank_address" name="bank_address" value="">
      </div>
    </div>

    <div class="form-group row">
      <label for="inputEmail" class="col-sm-4 col-form-label">Authorized Bank</label>
      <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Enter Authorized Bank" id="authorized_bank" name="authorized_bank" value="">
      </div>
    </div>

    <div class="form-group row">
      <label for="inputEmail" class="col-sm-4 col-form-label">Branch Details</label>
      <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Enter Branch Details" id="branch_details" name="branch_details" value="">
      </div>
    </div>  
</form>