<div class="modal fade" id="assign_module_modal" style="display:none">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Assign modules</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
           
            <div class="modal-body">
                <ul class="js-errors"></ul>
                <form role="form" name="assign-module-form" id="assign-module-form" novalidate="novalidate" action="javascript:void(0)" method="post">
                    <input type="hidden" id="module_group_id" value="" name="module_group_id" />
                    <input type="hidden" id="process_type" value="1" name="process_type" />
                    <div class="form-group clearfix">
                        {!!buildMenu2($menus->toArray())!!}
                    </div>
                    <div class="row right">
                        <input type="submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@php
    function buildMenu2($tree, $hasChild = 'false'){
        $uniqid = uniqid();
        $result = "";
        if(is_array($tree) && count($tree) > 0) {
            //$result .= '<ul class="">';
            foreach($tree as $node) {
                $down_arrow = '';
                
                $moduleClasses = '';
                if($node['is_default'] == 1){
                    $item_type = 'parent';
                    $moduleClasses = ' ParentModuleClass';
                    $doDisable = '';
                }elseif($node['is_default'] == 2){
                    $item_type = 'child';
                    $moduleClasses = ' ChildModuleClass ChildModuleClass'.$node['menu_id'];
                    $doDisable = 'disabled';
                }elseif($node['is_default'] == 3){
                    $item_type = 'innerPage';
                    $moduleClasses = ' innerPage innerPage'.$node['menu_id'];
                    $doDisable = 'disabled';
                }

                
                $parent_id = isset($node['parent_id']) ? $node['parent_id'] : 0;

                $result .= '<ul class="module-ul"><li><dl class=""><dt class="parent-dt icheck-success"><input class="moduleclass'.$moduleClasses.'" type="checkbox" name="buttoncheckbox[]" id="buttoncheckbox'.$node['id'].$uniqid.'" data-parent="'.$parent_id.'"  data-uniqueid="'.uniqid().'" value="'.$node['id'].'" data-type="'.$item_type.'Module" data-element="'.$item_type.'" '.$doDisable.'>
                <label for="buttoncheckbox'.$node['id'].$uniqid.'">'.$node['menu_name'].'</label></dt><dt class="has-child '.$hasChild.'">'.tabs_tree($node['tab_json'],$node['button_json'],$node['id'],$item_type).'</dt>';
                if(isset($node['children']) && count($node['children']) > 0){
                    $hasChild = count($node['children']);
                    $result .= '<ul class="module-ul child"><li>'.buildMenu2($node['children'], $hasChild).'</li></ul>';
                    if(isset($node['inner_pages']) && count($node['inner_pages']) > 0){
                        $hasChild = count($node['inner_pages']);
                        $result .= '<ul class="module-ul child"><li>'.buildMenu2($node['children'], $hasChild).'</li></ul>';
                    }
                }
                if(isset($node['inner_pages']) && count($node['inner_pages']) > 0){
                    $hasChild = count($node['inner_pages']);
                    $result .= '<ul class="module-ul child"><li>'.buildMenu2($node['inner_pages'], $hasChild).'</li></ul>';
                }
                $result .= '</dl></li></ul>';
            }
            //$result .= '</dl></li></ul>';
        }
        return $result;
    }

    function tabs_tree($tabs, $buttons, $id,$item_type){
        $result = '';
        if(!empty($buttons)){
            $result .= '<div class="access-wrapper"><div class="col-md-1"><label class="tab-outline-success outline-buttons">BUTTONS</label></div>';
            $buttons = (array)json_decode($buttons);
            $i = 0;
            foreach($buttons as $button){
                $uniqid = uniqid();
                $result .= '<div class="icheck-success mr-2"><input type="checkbox" value="'.$button.'" id="'.$uniqid.'" class="moduleclassT ml-2 '.$item_type.$id.'" data-parentId ="'.$id.'" data-type="'.$item_type.'ModuleButton" data-element="button" disabled><label for="'.$uniqid.'">'.strtoupper($button).'</label></div>';
                $i++;
            }
            $result .= '</div>';
        }

        if(!empty($tabs)){
            $result .= '<div class="access-wrapper"><div class="col-md-1"><label class="tab-outline-success outline-buttons">TABS</label></div>';
            $tabs = (array)json_decode($tabs);
            foreach($tabs as $tab){
                $uniqid = uniqid();
                $result .= '<div class="icheck-success mr-2"><input type="checkbox" value="'.$tab.'" id="'.$uniqid.'" class="moduleclassT ml-2 '.$item_type.$id.'" data-parentId ="'.$id.'" data-type="'.$item_type.'ModuleTab" data-element="tab" disabled><label for="'.$uniqid.'">'.strtoupper($tab).'</label></div>';
            }
            $result .= '</div>';
        }
        return $result;
    }
@endphp