
<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
  <h6> Account Tagging Details</h6>
</div>
<form class="form-horizontal update-account-form account_tagging_form" id="update-account-tagging-form" action="javascript:void(0)">  

@if((Route::currentRouteName() == 'get-mason-information' || Route::currentRouteName() == 'get-engineer-information' || Route::currentRouteName() == 'get-petty-contractor-information'))
<div class="form-group row">
  <label for="inputName2" class="col-sm-4 col-form-label"> Account Tagging Type </label>
  <div class="col-sm-8">
    <div class="icheck-primary d-inline">
        <input type="radio" id="account_tagging_type1" value="1" name="account_tagging_type" @if(!($details->subdealer_id > 0)) checked @endif>
        <label for="account_tagging_type1">Dealer</label>
    </div>
    <div class="icheck-primary d-inline">
        <input type="radio" id="account_tagging_type2" value="2" name="account_tagging_type" @if(($details->subdealer_id > 0)) checked @endif >
        <label for="account_tagging_type2">Sub Dealer</label>
    </div>
  </div>
</div>
@endif
@if((Route::currentRouteName() == 'get-dealer-information') || (Route::currentRouteName() == 'get-ihb-owner-information'))    
    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> Distributor</label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select Distributor" id="distributor_tagged_id" name="distributor_tagged_id">
          <option value=""></option>
          @foreach($distributor_list as $distributor_info)
              <option value="{{$distributor_info->distributor_id}}" @if($distributor_info->distributor_id == $details->distributor_id) selected @endif>{{$distributor_info->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
@endif
@if((Route::currentRouteName() == 'get-mason-information' || Route::currentRouteName() == 'get-engineer-information' || Route::currentRouteName() == 'get-petty-contractor-information' || Route::currentRouteName() == 'get-subdealer-information' || Route::currentRouteName() == 'get-ihb-owner-information'))
    <div class="form-group row" id="dealer_show">
      <label for="inputName2" class="col-sm-4 col-form-label"> Dealer </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select Dealer" id="dealer_tagged_id" name="dealer_tagged_id">
          <option value=""></option>
          @foreach($dealer_list as $dealer_info)
              <option value="{{$dealer_info->dealer_id}}" @if($dealer_info->dealer_id == $details->dealer_id) selected @endif>{{$dealer_info->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
@endif
@if((Route::currentRouteName() == 'get-mason-information' || Route::currentRouteName() == 'get-engineer-information' || Route::currentRouteName() == 'get-petty-contractor-information' || Route::currentRouteName() == 'get-ihb-owner-information'))    
    <div class="form-group row" id="subdealer_show" @if((Route::currentRouteName() == 'get-mason-information' || Route::currentRouteName() == 'get-engineer-information' || Route::currentRouteName() == 'get-petty-contractor-information')) @if(!($details->subdealer_id > 0)) style="display:none;"@endif @endif>
      <label for="inputName2" class="col-sm-4 col-form-label"> Sub Dealer </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select Subdealer" id="subdealer_tagged_id" name="subdealer_tagged_id">
          <option value=""></option>
          @foreach($subdealer_list as $subdealer_info)
              <option value="{{$subdealer_info->subdealer_id}}" @if($subdealer_info->subdealer_id == $details->subdealer_id) selected @endif>{{$subdealer_info->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
@endif
    <div class="blue-btn-wrap text-right">
            <input type="submit" class="btn btn-primary data-update" id="account_tagging" data-type="account_tagging" value="Update">
        </div>
    </form>