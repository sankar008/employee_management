<script>
    /* convert to account */
    $('#convert-to-account').on('click', function(){
        // var type        = {{$all_lead_info->lead_account_type_id}}
        // var isConverted = $(this).attr('data-type')

        // if(isConverted == 'false' && (type == 34 || type == 40)){
        //     $.ajax({
        //         type: 'GET',
        //         headers: {
        //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        //                     'Authorization': 'Bearer '+ BEARER
        //                 },
        //         data: {type,isConverted,doActive,data},
        //         complete : function (paylaod) {

        //         }
        //     })
        // }
        //show modal
        $('#modal-conversion-render').modal('show')
    })


    $("input[name='doActive']").on('click', function(){
        var doActive = $(this).val()
        if(doActive == 1){
            $('.sap-code-div').css('display','inline-flex')
        }else{
            $('.sap-code-div').css('display','none')
        }
    })

    /* restrict sap code length */
    $('.sap-number').keypress(function(){
        if($(this).val().length >= 8){
            return false
        }else{
            return true
        }
    })


    /* form validation */
    $('#account-conversion-form').validate({
        rules: {
            @if($all_lead_info->lead_account_type_id == 39 || $all_lead_info->lead_account_type_id == 40 || $all_lead_info->lead_account_type_id == 18)
            sold_to_sap: {
                required: function(element) {
                    if ($("input[name='doActive']:checked").val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            payer_to_sapid: {
                required: function(element) {
                    if ($("input[name='doActive']:checked").val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            ship_to_sapid: {
                required: function(element) {
                    if ($("input[name='doActive']:checked").val() == 1) {
                        return true;
                    } else {
                        return false;
                    }
                }
            },
            @endif
            //additional code for other accounts
        },
        messages: {
            @if($all_lead_info->lead_account_type_id == 39 || $all_lead_info->lead_account_type_id == 40 || $all_lead_info->lead_account_type_id == 18)
            sold_to_sap: {
                required: 'Sold to SAP id is required'
            },
            payer_to_sapid: {
                required: 'Pay to SAP id is required'
            },
            ship_to_sapid: {
                required: 'Ship to SAP id is required'
            },
            @endif
            //additional code for other accounts
        },
        submitHandler: function(){
            var doActive  = $("input[name='doActive']:checked").val()
            var lead_type = '{{$all_lead_info->lead_account_type_id}}'
           // alert(lead_type);
            if(lead_type == 18 || lead_type == 39 || lead_type == 40){
                var url  = '""';
                var sold_to_sapid  = 0
                var ship_to_sapid  = 0
                var payer_to_sapid = 0
                var associate_id = 0
                if($('#associate_id').val() != '' && (lead_type == 34 || lead_type == 40)){
                    var associate_id = $('#associate_id').val()
                }

                if(doActive == 1){
                    var sold_to_sapid  = $("#sold_to_sap").val()
                    var ship_to_sapid  = $("#ship_to_sapid").val()
                    var payer_to_sapid = $("#payer_to_sapid").val()
                }

                var data = {
                        sold_to_sapid: sold_to_sapid,
                        ship_to_sapid: ship_to_sapid,
                        payer_to_sapid: payer_to_sapid,
                        associate_id: associate_id
                }
                var data = JSON.stringify(data)
            }else if(lead_type == 45 || lead_type == 44 || lead_type == 46){
                //for other accounts convert-to-influncer-account
                var url  = '{{route("convert-to-influncer-account")}}';
            }
            else if(lead_type == 47)
            {
                var url  = '{{route("convert-to-ihb-account")}}';
            }
            //ajax call
            $.ajax({
                    url:  url,
                    type: 'POST',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                'Authorization': 'Bearer '+ BEARER
                            },
                    data: {lead_type,lead_id,doActive,data},
                    complete : function (paylaod) {
                        if(paylaod.status == 201){
                            toastr.success('Account converted successfully')
                            $('#modal-conversion-render').modal('hide')
                            $('#convert-to-account').prop('disabled', true)
                        }else if(paylaod.status == 409){
                            toastr.warning('Account already exist with some duplicate data')
                        }else if(paylaod.status == 204){
                            toastr.warning('Information missing')
                        }else{
                            toastr.error('Something went wrong please try again later')
                        }
                    }
            });
        }
    })


</script>
