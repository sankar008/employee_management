    <script>
        /* enable responible employee */
        var currentroute = '{{ Route::currentRouteName() }}';
        $('#selftask_assign').on('change',function(){
            if($(this).prop("checked") == true){
                $('#resp_emp_id').val([]).trigger('change');
                $('#resp_emp_id').prop('disabled', true);
            }else{
                $('#resp_emp_id').removeAttr('disabled');
            }
        });

        $("#add-task-lead-form").validate({
            rules:
            {
                task_lead_id: {
                    required: true
                },
                resp_emp_id: {
                        required: function(element) {
                        return $('#selftask_assign').is(':not(:checked)') == true
                        }
                },
                task_type_id: {
                    required: true
                },
                task_date: {
                    required: true,
                    //greaterThan: "#task_date",
                },
                task_title: {
                    required: true,
                },
                task_description: {
                    required: true
                },
                task_priority_id: {
                    required: true
                }
            },
            messages: {
                task_lead_id: {
                    required: 'Please Select Lead List'
                },
                resp_emp_id: {
                    required: 'Please Select Employee List'
                },
                task_type_id: {
                    required: 'Please Select task type List'
                },
                task_date: {
                    required: 'Please Enter Task date',
                    //greaterThan: "Task date should not equal to today's data",
                    //equalTo: "task_date equal",
                },
                task_title: {
                    required: 'Please Enter Task Subject'
                },
                task_description: {
                    required:'Please type task description'
                },
                task_priority_id: {
                    required:'Please select task priority'
                }
            },
            submitHandler: function(form) 
            {
                var lead_id         = $('#task_lead_id').val();
                var submitbutton    = $(this.submitButton).attr('data-pid');
                var data            = $('#add-task-lead-form').serializeArray();
                dataObj      = {};var product_array = [];
                $(data).each(function(i, field)
                {
                    dataObj[field.name] = field.value;
                    if(field.name == 'lead_remarks')
                    {
                        dataObj['lead_remarks'] = field.value;
                    }
                    if(field.name == 'task_lead_id')
                    {
                        dataObj['lead_id'] = field.value;
                    }
                    
                });
                var dataArray = {
                    'userdata' : dataObj,
                    'product_array' :product_array,
                };
                var data = JSON.stringify(dataArray);
                $.ajax({
                    url: '{{route("submit-task-lead")}}',
                    type: 'POST',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                'Authorization': 'Bearer '+ BEARER
                    },
                    data: {data,lead_id},
                    complete: function(payload){
                        var newData = payload.responseText;
                        if(payload.status == 201){
                            toastr.success('New Task has been added');
                            if(currentroute == 'lead-list')
                            {
                                $('#lead-list-section').DataTable().destroy();
                                fetch_data();
                                if(submitbutton == 1){
                                    $('#add-task-lead-modal').modal('hide');
                                }
                            }else if(currentroute == 'get-lead-information'){
                                $('#table-followup-self').DataTable().destroy();
                                $('#table-followup-emp').DataTable().destroy();
                                render_self_followup();
                                render_emp_followup();
                                if(submitbutton == 1){
                                    $('#modal-self').modal('hide');
                                }
                            }else{
                                $('#lead-list-assignedtask').DataTable().destroy();
                                if($('#selftask_assign').is(':checked') == true){
                                    fetch_data(1,'lead-list-mytask');
                                }else
                                {
                                    fetch_data(2,'lead-list-assignedtask');
                                }

                                if(submitbutton == 1){
                                    $('#add-task-lead-modal').modal('hide');
                                }
                            }
                            /* reset the form */
                            $('#add-task-lead-form input').removeClass('error');
                            $('#add-task-lead-form select').removeClass('error');
                            $('#add-task-lead-form input').trigger("reset");
                            $("#add-task-lead-form")[0].reset();
                            $(".reset_dropdown").val([]).trigger("change"); 
                            $('.task_date').val(getCurrentDateTime());
                            $('#selftask_assign').prop('disabled', false);
                            $('#resp_emp_id').prop('disabled', false);

                        }else if(payload.status == 207){
                            toastr.info('Task already available for ' + payload.responseJSON.data.date);
                        }else if(payload.status == 400){
                            toastr.error('Bad request');
                        }else{
                            toastr.error('Something went wrong please try again later.');
                        }
                    }
                });
            },
        });

        /* validation for reamrks form */
    $("#add-reamrks-form").validate({
        rules:{
            remarkT: {
            required: true
            },
            rescheduled_date: {
                        required: function(element) {
                        return $('#rescheduled').is(':checked') == true
                        }
            }
        },
        messages: {
            remarkT: {
            required: 'Please type your remark'
            },
            rescheduled_date: {
            required: 'Please select date'
            }
        },
        submitHandler: function() {
            var id     = $('#activity_id').val();
            var remark = $('textarea#remarkT').val();
            var date   = $('#rescheduled_date').val();
            var followup_type  = $('#followup_type').val();
            if($('#account_confirm').prop("checked") == true){
                var account_confirm = 1;
            }else{
                var account_confirm = 0;
            }

            if($('#rescheduled').prop("checked") == true){
                var isRescheduled = 1;
            }else{
                var isRescheduled = 0;
            }
                
            $.ajax({
                    url: '{{route("remarks-add")}}',
                    type: 'POST',
                    headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                            'Authorization': 'Bearer '+ BEARER
                    },
                    data: {id, remark, isRescheduled, date, followup_type, account_confirm},
                    complete: function(payload, xhr, settings){
                        if(payload.status == 201 || payload.status == 200){
                            $('#rescheduled').prop('checked',false);
                            $('#account_confirm,#rescheduled_date').prop('disabled',true);
                            $('#remarkT').val('');
                            $(".reset_dropdown").val([]).trigger("change");
                            $('#modal-remarks, #create-followup-lead-modal').modal('hide');
                            $('#rescheduled_date').val(getCurrentDateTime());
                            if(payload.status == 201){
                                toastr.success('Task rescheduled');
                            }else{
                                toastr.success('Remarks added for task');
                            }
                        }else if(payload.status == 403){
                            toastr.error('You can\'t rescheduled task more than 3 times');
                        }else{
                            toastr.error('Something went wrong, Please try again later.');
                        }
                        if(currentroute == 'get-lead-information'){
                            $('#table-followup-self').DataTable().destroy();
                            render_self_followup();
                            $('#table-followup-emp').DataTable().destroy();
                            render_emp_followup();
                            format();
                        }
                    }
            });
        }
    });

    /* delete task */
    function get_delete(delete_id)
    {
        Swal.fire({
            title: 'Are you sure?',
            text: "you want to delete this task!",
            input: 'textarea',
            inputPlaceholder: 'Reason for delete',
            icon: 'warning',
            inputValidator: (value) => {
                if (!value) {
                    return 'Please specify your reason to delete!'
                }
            },
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
            if (result.value) {
                var reason = result.value;
                var type = 'LeadTask';
                //console.log(result);
                $.ajax({
                    url: '{{route("get-task-delete")}}',
                    type: 'DELETE',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                'Authorization': 'Bearer '+ BEARER
                    },
                    data: {delete_id,reason,type},
                    complete: function(payload, xhr, settings){
                        //console.log(payload);
                        var newData = payload.responseText;
                        if(payload.status == 200){
                            Swal.fire(
                            'Deleted!',
                                'Your task has been deleted.',
                                'success'
                            );
                        }else if(payload.status == 403){
                            toastr.error('OOps!! you can"\t delete this task');
                        }else{
                            toastr.error('Something went wrong, Please try again later.');
                        }
                        if(currentroute == 'lead-followup'){
                            var tab = $('.info-tab-header .nav-link.active').attr('pid');
                            var tab = tab.split('@')[0];
                            if(tab == 1){
                                var T = 'lead-list-mytask';
                            }else{
                                var T = 'lead-list-assignedtask';
                            }
                            fetch_data(tab,T);
                        }else{
                            render_self_followup();
                            render_emp_followup();
                        }
                        
                    }
                });
            }
        });
    }
    /* reset rescheduled checkbox and other fields on modal close */

    $('#create-followup-lead-modal, #modal-remarks').on('hidden.bs.modal', function (e) {
        $('#rescheduled').prop('checked',false);
        $('#account_confirm').prop('disabled',true);
        $('#rescheduled_date').val(getCurrentDateTime());
        $('#remarkT').val('');
        $(".reset_dropdown").val([]).trigger("change");
    })
    

    $("#resp_emp_show").hide();
    /* edit task */
    $(document).on('click','.edit-icons',function(){
        var id = $(this).attr('data-id')
        var url = '{{ route("get-task-info", ":id") }}'
        url = url.replace(':id', id)
        $.ajax({
                url: url,
                type: 'GET',
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                            'Authorization': 'Bearer '+ BEARER
                },
                data: {},
                complete: function(payload){
                    if(payload.status == 200){
                        var newData = payload.responseJSON
                        var newData = newData.data
                        console.log(newData.user_id)
                        $("#resp_emp_show").hide()
                        if(newData.user_id != newData.assign_by){
                            $("#resp_emp_show").show()
                            $(".resp_emp_id").val([newData.user_id]).trigger('change')
                        }
                        $('.task_priority_id').val([newData.task_priority_id]).trigger('change')
                        $('.task_type_id').val([newData.followup_type]).trigger('change')
                        $('.task_date').val(newData.followup_date)
                        $('.task_purpose_id').val([newData.task_purpose_id]).trigger('change')
                        $('.task_subject').val([newData.task_subject])
                        $('.task_description').val(newData.task_description)
                        $('#task_id').val(id)
                        $('.select2bs4').select2({
                            theme: 'bootstrap4',
                            disabled:true
                        });
                        $('#edit-lead-task-modal').modal('show');
                    }else if(payload.status == 204){
                        toastr.info('Sorry we didn"\t find any information with this task')
                    }else{
                        toastr.error('Something went wrong please try again later')
                    }
                }
        });
    });

    /* small validation for edit task */
    $(".add-task-lead-form").validate({
        rules:{
            task_title: {
                required: true
            },
            task_description: {
                required: true
            }
        },
        messages: {
            task_title: {
                required: 'Please Enter Task Subject'
            },
            task_description: {
                required:'Please type task description'
            },
        },
        submitHandler: function() {
            var id = $('#task_id').val()
            var title = $('.task_subject').val()
            var description = $('.task_description').val()
            $.ajax({
                url: '{{route("update-followup-task")}}',
                type: 'PATCH',
                headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                        'Authorization': 'Bearer '+ BEARER
                },
                data: {id,title,description},
                complete: function(payload, xhr, settings){
                    if(payload.status == 200){
                        toastr.success('Task updated successfully')
                        $('#edit-lead-task-modal').modal('hide')
                        if(currentroute == 'lead-followup'){
                            var tab = $('.info-tab-header .nav-link.active').attr('pid');
                            var tab = tab.split('@')[0];
                            if(tab == 1){
                                var T = 'lead-list-mytask';
                            }else{
                                var T = 'lead-list-assignedtask';
                            }
                            fetch_data(tab,T);
                        }else if(currentroute == 'get-lead-information'){
                            render_self_followup();
                            render_emp_followup();
                        }
                    }else{
                        toastr.error('Something went wrong please try again later')
                    }
                }
            })
        }
    });
    /* reset modal for edit task */
    $('#edit-lead-task-modal').on('hidden.bs.modal', function () {
        $('.select2bs4').select2({
            theme: 'bootstrap4',
            disabled:false
        });
    })
    </script>
