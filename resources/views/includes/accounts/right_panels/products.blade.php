<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
    <h6>Product & Services</h6>
</div>
<form action="javascript:void(0)" class="form-horizontal update-account-form product_preferences_form">
    <div class="form-group row">
        <label for="inputExperience" class="col-sm-4 col-form-label">Products </label>
        <div class="col-sm-8">
          <select class="form-control select2bs4 products_dropdown" data-placeholder="Select product(s)" multiple id="products" name="products[]" value="{!!json_encode($details->products)!!}">
            <option></option>
            @if (!$products->isEmpty())
                @foreach ($products as $product)
                <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                @endforeach
            @endif
          </select>
          <input type="hidden" id="updated_products" value="">
          <div class="row custom_radio_class"><label for="products" class="error" style="display:none;"></label></div>
        </div>
    </div>
    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
    <div class="blue-btn-wrap text-right">
        <input type="submit" class="btn btn-primary data-update" id="product_preferences" data-type="product_preferences" value="Update">
    </div>
    @endif
</form>
