
<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
    <h6> Basic Information</h6>
</div>
<form class="form-horizontal update-account-form basic_information_form" id="basic_information_form" action="javascript:void(0)">
    <div class="form-group row">
    <label for="inputName" class="col-sm-4 col-form-label">@if(Route::currentRouteName() == 'account.fabricators.info') Fabricator Name @endif @if(Route::currentRouteName() == 'account.dealer.info') Dealer Name @endif @if(Route::currentRouteName() == 'account.distributor.info') Distributor Name @endif @if(Route::currentRouteName() == 'account.subdealer.info') Subdealer Name @endif  @if(Route::currentRouteName() == 'account.masons.info') Mason Name @endif @if(Route::currentRouteName() == 'account.engineers.info') Engineer Name @endif @if(Route::currentRouteName() == 'account.pettycontractors.info') PC Name @endif @if(Route::currentRouteName() == 'get-ihb-owner-information') Owner Name @endif <span class="required_star">*</span></label>
    <div class="col-sm-8">
        <input type="text" class="form-control toUpperCase" placeholder="Enter shop/ establishment/ individual name" id="account_name" name="account_name" placeholder="Name" value="{{ $details->account_name }}">
    </div>
    </div>
    @if(!(Route::currentRouteName() == 'account.masons.info' || Route::currentRouteName() == 'account.engineers.info' || Route::currentRouteName() == 'account.pettycontractors.info' || Route::currentRouteName() == 'get-ihb-owner-information'))
    <div class="form-group row">
    <label for="inputName" class="col-sm-4 col-form-label">Contact Person <span class="required_star">*</span></label>
    <div class="col-sm-8">
        <input type="text" class="form-control toUpperCase" placeholder="Enter wwner's name" id="contact_name" name="contact_name" placeholder="Contact Name" value="{{$details->contact_person}}">
    </div>
    </div>
    @endif


    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Email</label>
    <div class="col-sm-8">
        <input type="email" class="form-control"  placeholder="Enter email address" id="email" name="email" value="{{$details->email}}">
    </div>
    </div>

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Mobile No</label>
    <div class="col-sm-8">
        <input type="text" class="form-control only-number"  placeholder="Enter mobile number" id="mobile_no" name="mobile_no" value="{{$details->mobile_no}}">
    </div>
    </div>


    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Alternate No</label>
    <div class="col-sm-8">
        <input type="text" class="form-control only-number"  placeholder="Alternate contact number" id="alternate_mobile_no" name="alternate_mobile_no" value="@if($details->alternate_no > 0){{$details->alternate_no}}@endif">
    </div>
    </div>

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">WhatsApp No</label>
    <div class="col-sm-8">
        <input type="text" class="form-control only-number"  placeholder="Enter WhatsApp number" id="whatsapp_no" name="whatsapp_no" value="@if($details->whatsapp_no > 0){{$details->whatsapp_no}}@endif">
    </div>
    </div>

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Landline No</label>
    <div class="col-sm-8">
        <input type="text" class="form-control only-number"  placeholder="Enter land line number" id="land_line_no" name="land_line_no" value="@if($details->landline_no > 0){{$details->landline_no}}@endif">
    </div>
    </div>

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Date of Birth</label>
    <div class="col-sm-8">
        <input type="text" class="form-control basic_datepicker"  placeholder="Select date of birth" readonly id="dob" name="dob" value="@if($details->dob != NULL && $details->dob != '0000-00-00' && $details->dob != '1970-01-01'){{date('d-m-Y',strtotime($details->dob))}}@endif">
    </div>
    </div>

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Date of Anniversary </label>
    <div class="col-sm-8">
        <input type="text" class="form-control basic_datepicker"  placeholder="Select date of anniversary" id="date_of_anniversary" name="date_of_anniversary" value="@if($details->doa != NULL && strtotime($details->doa) > 0 && $details->doa != '1970-01-01'){{date('d-m-Y',strtotime($details->doa))}}@endif" readonly>
    </div>
    </div>

    @if(in_array($account_type, [18,39]))
    <div class="form-group row d-none">
    <label for="inputEmail" class="col-sm-4 col-form-label">SAP Code</label>
    <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Enter SAP code" id="sap_id" name="sap_id" value="{{$details->sap_code}}" readonly>
    </div>
    </div>
    @endif

    <div class="form-group row">
    <label for="inputEmail" class="col-sm-4 col-form-label">Referred By</label>
    <div class="col-sm-8">
        <input type="text" class="form-control"  placeholder="Referred by" id="referral_by" name="referral_by" value="" readonly>
    </div>
    </div>

    @if ($account_type == 40 || $account_type == 39)
    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Parent @if($account_type == 39) {{"Distributor"}} @elseif ($account_type == 40) {{"Dealer"}} @endif</label>
        <div class="col-sm-8">
            <select class="form-control select2bs4" data-placeholder="Select parent account" id="account_parent" name="account_parent">
                <option></option>
                @if (!$all_mappable_accounts->isEmpty())
                    @foreach ($all_mappable_accounts as $eachAccount)
                        <option value="{{$eachAccount->account_id}}" @if($details->parent_account_id == $eachAccount->account_id) {{'selected'}} @endif>{{$eachAccount->name.' ('.$eachAccount->sap_code.') ('.ucwords($eachAccount->status_code->type_name).')'}}</option>
                    @endforeach
                @endif
            </select>
        </div>
    </div>
    @endif

    @if ($account_type == 39)
    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">SAP activation date</label>
        <div class="col-sm-8">
            <input type="text" class="form-control basic_datepicker"  placeholder="Select SAP activation date" id="sap_activation_date" name="sap_activation_date" value="@if($details->sap_activation_date != NULL && strtotime($details->sap_activation_date) > 0 && $details->sap_activation_date != '1970-01-01'){{date('d-m-Y',strtotime($details->sap_activation_date))}}@endif" readonly>
        </div>
    </div>
    @endif

    @if ($account_type == 44)
    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Engineer Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="engineer_type" id="engineer_type"  class="form-control select2bs4" data-placeholder="Select engineer type">
                <option></option>
                <option value="2" @if(isset($meta_tags['engineer_type']) && $meta_tags['engineer_type'][2] == 2) selected  @endif>Government engineer</option>
                <option value="1" @if(isset($meta_tags['engineer_type']) && $meta_tags['engineer_type'][2] == 1) selected  @endif>Private engineer</option>
            </select>
            <div class="row custom_radio_class"><label for="engineer_type" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Office Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="engineer_office_type_id" id="engineer_office_type_id"  class="form-control select2bs4 hasOtherValue" data-pair="engineer_office_type_name" data-placeholder="Select engineer type">
                <option></option>
                @foreach (collect($office_types) as $office_type)
                <option value="{{$office_type['id']}}" @if(isset($meta_tags['engineer_office_type_id']) && is_numeric($meta_tags['engineer_office_type_id'][2])  && $meta_tags['engineer_office_type_id'] != null && $meta_tags['engineer_office_type_id'][2] == $office_type['id']) selected  @endif>{{$office_type['name']}}</option>
                @endforeach
            </select>
            <div class="row custom_radio_class"><label for="engineer_office_type_id" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Office Name </label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase"  placeholder="Office Name" id="engineer_office_type_name" name="engineer_office_type_name" @isset($meta_tags['engineer_office_type_name'][2]) value="{{$meta_tags['engineer_office_type_name'][2]}}" @endisset @if(isset($meta_tags['engineer_office_type_id']) && $meta_tags['engineer_office_type_id'][2] != 0 || !isset($meta_tags['engineer_office_type_name'][2]) || empty($meta_tags['engineer_office_type_name'][2])) readonly @endif>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Designation Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="engineer_designation_id" id="engineer_designation_id"  class="form-control select2bs4 hasOtherValue" data-pair="engineer_designation_name" data-placeholder="Select engineer designation">
                <option></option>
                @foreach (collect($designations) as $designation)
                <option value="{{$designation['id']}}" @if(isset($meta_tags['engineer_designation_id']) && is_numeric($meta_tags['engineer_designation_id'][2]) && $meta_tags['engineer_designation_id'][2] == $designation['id']) selected  @endif>{{$designation['name']}}</option>
                @endforeach
            </select>
            <div class="row custom_radio_class"><label for="engineer_designation_id" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Designation </label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase"  placeholder="Designation" id="engineer_designation_name" name="engineer_designation_name" @isset($meta_tags['engineer_designation_name'][2]) value="{{$meta_tags['engineer_designation_name'][2]}}" @endisset @if(isset($meta_tags['engineer_designation_id']) && $meta_tags['engineer_designation_id'][2] != 0 || !isset($meta_tags['engineer_designation_name'][2]) || empty($meta_tags['engineer_designation_name'][2])) readonly @endif>
        </div>
    </div>

    @endif

    @if ($account_type == 45)
    <div class="form-group row">
        <label for="inputEmail" class="col-sm-4 col-form-label">Mason Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="mason_type" id="mason_type"  class="form-control select2bs4" data-placeholder="Select mason type">
                <option></option>
                <option value="RAJ MISTRY" @if(isset($meta_tags['mason_type']) && $meta_tags['mason_type'][2] == "RAJ MISTRY") selected  @endif>RAJ MISTRY</option>
                <option value="LABOUR" @if(isset($meta_tags['mason_type']) && $meta_tags['mason_type'][2] == "LABOUR") selected  @endif>LABOUR</option>
            </select>
            <div class="row custom_radio_class"><label for="mason_type" class="error" style="display:none;"></label></div>
        </div>
    </div>
    @endif

    @if ($account_type == 44 || $account_type == 45 || $account_type == 46)
    <div class="form-group row">
        <label for="inputName" class="col-sm-4 col-form-label">Parent Account Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="parent_account_type" id="parent_account_type"  class="form-control select2bs4" data-placeholder="Select parent account type" data-bind="parent_account_id">
                <option></option>
                <option value="39">DEALER</option>
                <option value="40">SUBDEALER</option>
            </select>
            <div class="row custom_radio_class"><label for="parent_account_type" class="error" style="display:none;"></label></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName" class="col-sm-4 col-form-label">Parent Account Name <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select name="parent_account_id" id="parent_account_id" class="form-control select2bs4" data-placeholder="Select Account">
                <option value=""></option>
            </select>
            <div class="row custom_radio_class"><label for="parent_account_id" class="error" style="display:none;"></label></div>
        </div>
    </div>
    @endif

    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
    <div class="blue-btn-wrap text-right">
        <input type="submit" class="btn btn-primary data-update" id="basic_information" data-type="basic_information" value="Update">
    </div>
    @endif
</form>
