<div class="lead-form-title col-md-12 pt-3"><h6>Responsible @if(in_array($account_type, [39, 40])) {{' & Appointed'}} @endif Employee</h6></div>
<div class="form-horizontal">
    <form action="javascript:void(0)" class="update-lead-form responsible_employee_form">
        <div class="form-group row">
            <label for="inputEmail" class="col-md-3 col-form-label">SO</label>
            <div class="col-sm-9 text-primary font-weight-bold">
                {{$employees['so'] ? $employees['so'] : 'Not available'}}
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail" class="col-md-3 col-form-label">TO</label>
            <div class="col-sm-9 text-primary font-weight-bold">
                {{$employees['te'] ? $employees['te'] : 'Not available'}}
            </div>
        </div>

        <div class="form-group row">
            <label for="inputEmail" class="col-md-3 col-form-label">DTO/DSO</label>
            <div class="col-sm-9 text-primary font-weight-bold">
               {{$employees['dso'] ? $employees['dso'] : 'Not available'}}
            </div>
        </div>
    </form>

    @if(in_array($account_type, [39, 40]))
    <form action="javascript:void(0)" class="update-lead-form appointed_employee_form mt-2">
        <div class="form-group row">
            <label for="inputEmail" class="col-md-3 col-form-label">Appointed Employee</label>
            <div class="col-sm-9">
            <select name="appointed_employee" id="appointed_employee" class="form-control select2bs4" multiple data-placeholder="Select employee">
                <option value=""></option>
                @if (!$appointees->isEmpty())
                        @foreach ($appointees as $employee)
                            <option value="{{ $employee->id }}">{{ strtoupper($employee->name) }} ( {{ strtoupper($employee->get_user_by_profile->profile_name) }} )</option>
                        @endforeach
                @endif
            </select>
            </div>
        </div>

        @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
        <div class="blue-btn-wrap text-right">
            <input type="submit" class="btn btn-primary data-update" id="__appointed_employee" data-type="appointed_employee" value="Update">
        </div>
        @endif
    </form>
    @endif
</div>
