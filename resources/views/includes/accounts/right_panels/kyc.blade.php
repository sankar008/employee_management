<div class="lead-form-title col-md-12 pt-3"><h6> KYC Information</h6></div>
<form action="javascript:void(0)" class="form-horizontal update-lead-form kyc_form">
    <div class="form-group row">
        <label for="inputName" class="col-sm-4 col-form-label">Charecter of Business <span class="required_star" name="business_body_type_label">*</span></label>
        <div class="col-sm-8 d-flex align-items-center preventDropdown">
            <select name="business_body_type" id="business_body_type" data-placeholder="Select business type" class="form-control select2bs4" value="{{$details->business_body_type}}">
                <option></option>
                <option value="1">Association of Persons (AOP)</option>
                <option value="2">Body of Individuals (BOI)</option>
                <option value="3">Company</option>
                <option value="4">Firm/Limited Liability Partnership</option>
                <option value="5">Government Agency</option>
                <option value="6">HUF (Hindu Undivided Family)</option>
                <option value="7">Local Authority</option>
                <option value="8">Artificial Judicial Person</option>
                <option value="9">Individual</option>
                <option value="10">Trust</option>
            </select>
        </div>
    </div>

    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">PAN No <span name="pan_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase" placeholder="Enter PAN number" id="pan_no" name="pan_no" value="@if(isset($kyc_info['pan_no'])){{$kyc_info['pan_no']}}@elseif(isset($details->pan_no)){{$details->pan_no}}@elseif(isset($extra_information['pan_no'])){{$extra_information['pan_no']}}@endif"">
        </div>
    </div>

    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">Trade license no <span name="tradel_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase" placeholder="Enter trade license number" id="tradel_no" name="tradel_no" value="@if(isset($kyc_info['tradel_no'])){{$kyc_info['tradel_no']}}@elseif(isset($details->tradel_no)){{$details->tradel_no}}@elseif(isset($extra_information['tradel_no'])){{$extra_information['tradel_no']}}@endif">
        </div>
    </div>

    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">GSTIN <span name="gst_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase" placeholder="Enter GSTIN number" id="gst_no" name="gst_no" value="@if(isset($kyc_info['gst_no'])){{$kyc_info['gst_no']}}@elseif(isset($details->gst_no)){{$details->gst_no}}@elseif(isset($extra_information['gst_no'])){{$extra_information['gst_no']}}@endif"">
        </div>
    </div>

    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">Aadhaar no <span name="aadhaar_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control only-number" placeholder="Enter aadhaar number" id="aadhaar_no" name="aadhaar_no" value="@if(isset($kyc_info['aadhaar_no'])){{$kyc_info['aadhaar_no']}}@elseif(isset($details->aadhaar_no)){{$details->aadhaar_no}}@elseif(isset($extra_information['aadhaar_no'])){{$extra_information['aadhaar_no']}}@endif">
        </div>
    </div>

    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">Voter/EPIC no <span name="epic_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase" placeholder="Enter voter/epic id card no" id="epic_no" name="epic_no" value="@if(isset($kyc_info['epic_no'])){{$kyc_info['epic_no']}}@elseif(isset($details->epic_no)){{$details->epic_no}}@elseif(isset($extra_information['epic_no'])){{$extra_information['epic_no']}}@endif">
        </div>
    </div>

    @if ($account_type == 44)
    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">EIN <span name="ein_no"></span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control toUpperCase" placeholder="Enter EIN number" id="ein_no" name="ein_no" value="@if(isset($kyc_info['ein_no'])){{$kyc_info['ein_no']}}@elseif(isset($details->ein_no)){{$details->ein_no}}@elseif(isset($extra_information['ein_no'])){{$extra_information['ein_no']}}@endif">
        </div>
    </div>
    @endif

    @if ($account_type == 39)
    <div class="form-group row col-md-6">
        <label for="inputEmail" class="col-sm-4 col-form-label">Additional KYC</label>
        <div class="col-sm-8">
            <textarea name="additional_kyc" id="additional_kyc" class="form-control toUpperCase" placeholder="Enter additional KYC data" cols="30" rows="5">@if(isset($details->additional_kyc) && !empty($details->additional_kyc)){{trim($details->additional_kyc)}}@endif</textarea>
        </div>
    </div>
    @endif
    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
    <div class="blue-btn-wrap text-right">
        <input type="submit" class="btn btn-primary data-update" id="kyc" data-type="kyc" value="Update">
    </div>
    @endif
</form>

<!-- upload modal -->
<div class="modal fade show" id="upload-modal" aria-modal="true" style="padding-right: 17px;">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Upload document</h4>
            </div>
            <div class="modal-body">
                <div class="col-md-12 d-flex p-0">
                    <input type="hidden" id="doc-type" value="">
                    <div class="col-sm-12">
                        <div class="dropzone dropzone-previews" id="kyc_files" name="kyc_files" style="border:2px dashed #0087F7;border-radius:4px;"></div>
                    </div>
                </div>
                <div class="blue-btn-wrap text-right">
                    <input type="submit" class="btn btn-primary upload-file" value="Save">
                </div>
            </div>
        </div>
    </div>
</div>


