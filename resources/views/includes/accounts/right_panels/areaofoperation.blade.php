<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
    <h6> Area of Operation</h6>
</div>
<form class="form-horizontal update-account-form areaofoperation_form" id="update-areaofoperation-form" action="javascript:void(0)">
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label ">State <span class="required_star">*</span></label>
        <div class="col-sm-8">
        <select class="form-control select2bs4 stateclass" data-placeholder="Select state" id="operation_state" name="operation_state[]" readonly>
            <option value=""></option>
            @foreach($states as $state)
                <option value="{{$state->state_id}}">{{$state->state_name}}</option>
            @endforeach
        </select>
        <input type="hidden" class="operational-states" value="[]">
        <div class="row custom_radio_class"><label for="state_name" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">District <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control select2bs4 operation_district" data-placeholder="Select district" id="operation_district" name="operation_district[]" multiple>
                <option value=""></option>
            </select>
            <input type="hidden" class="operational-districts" value="[]">
            <div class="row custom_radio_class"><label for="district_name" class="error" style="display:none;"></label></div>
        </div>
    </div>

    @if(!in_array($account_type, [44, 45, 46]))
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Block </label>
        <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select block" id="operation_block_id" name="operation_block_id[]" multiple>
            <option value=""></option>
        </select>
        <input type="hidden" class="operational-blocks" value="[]">
        <div class="row custom_radio_class"><label for="block_name" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Area Type</label>
        <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select area type" id="operation_area_type" name="operation_area_type">
            <option value=""></option>
           <option value="2">Rural</option>
           <option value="1">Urban</option>
        </select>
        <input type="hidden" class="operational-areatype" value="[]">
        <div class="row custom_radio_class"><label for="operation_area_type" class="error" style="display:none;"></label></div>
        </div>
    </div>

    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Area Name</label>
        <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select area name" id="operation_area" name="operation_area" >
            <option value=""></option>
        </select>
        <input type="hidden" class="operational-area" value="[]">
        <div class="row custom_radio_class"><label for="operation_area" class="error" style="display:none;"></label></div>
        </div>
    </div>
    @endif

    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
    <div class="blue-btn-wrap text-right">
        <input type="submit" class="btn btn-primary data-update" id="areaoperational" data-type="areaofoperation" value="Update">
    </div>
    @endif
</form>
