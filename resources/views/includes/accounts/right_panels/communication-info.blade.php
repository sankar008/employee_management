<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
    <h6> Communication Details</h6>
    <span class="addmore mb-2">
        <input type="hidden" name="total_address" id="total_address" value="@php echo isset($details->addresses_count) ?$details->addresses_count : 0 @endphp"   />
    </span>
</div>
<input type="hidden" class="address_json_store" value="">
<form class="form-horizontal pt-0 update-account-form communication_details_form" id="update-communication-details" action="javascript:void(0)">
    <div id="address_content_show"></div>
    <input type="hidden" name="removeid" id="removeid" value=""/>
    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
    <div class="blue-btn-wrap text-right">
        <input type="button" class="btn btn-success" name="add_more_button" id="add_more_button" value="Add More">
        <input type="submit" class="btn btn-primary data-update" id="communication_details" data-type="communication_details" value="Update">
    </div>
    @endif
</form>


<div class="div_wrapper col-md-12 row addnewaddress_clone hide border_top">
    <div class="class-cross removeVariant"><span><i class="fas fa-times"></i></span></div>
    <input type="hidden" class="addressid" name="addressid[]">
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Address Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET addresstypeclass" data-placeholder="Select address type" name="address_type[]" id="addresstype" value="">
                <option></option>
                <option value="1">Home</option>
                <option value="2" >Office</option>
                <option value="3" >Shop/Counter</option>
                <option value="4" >Godown</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label ">Address Line 1  </label>
        <div class="col-sm-8">
            <input type="text" class="form-control addressline1" placeholder="Address line 1" id="address1" name="address1[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Address Line 2 </label>
        <div class="col-sm-8">
            <input type="text" class="form-control addressline2" placeholder="Address line 2" id="address2" name="address2[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Post Office </label>
        <div class="col-sm-8">
            <input type="text" class="form-control post_office" placeholder="Post office" id="post_office" name="post_office[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Police Station </label>
        <div class="col-sm-8">
            <input type="text" class="form-control police_station" placeholder="Police station" id="police_station" name="police_station[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Landmark </label>
        <div class="col-sm-8">
            <input type="text" class="form-control landmark" placeholder="Landmark" id="nearlandmark" name="nearlandmark[]" value="">
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label ">State </label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET stateclass" data-placeholder="Select state" id="state" name="state[]" data-rowID="">
                <option></option>
            </select>
            <div class="row custom_radio_class">
                <label for="state_name" class="error" style="display:none;"></label>
            </div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">District <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET districtclass" data-placeholder="Select district" id="districts" name="address_district[]">
                <option></option>
            </select>
        <div class="row custom_radio_class"><label for="district_name" class="error" style="display:none;"></label></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Block <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET blockclass" data-placeholder="Select Block" id="blocks" name="address_block[]">
                <option></option>
            </select>
        <div class="row custom_radio_class"><label for="block_name" class="error" style="display:none;"></label></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Area Type <span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET areatypeclass" data-placeholder="Select district" id="areatype" name="address_areatype[]">
                <option></option>
            </select>
        <div class="row custom_radio_class"><label for="areatype_name" class="error" style="display:none;"></label></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label">Area Name<span class="required_star">*</span></label>
        <div class="col-sm-8">
            <select class="form-control appendSelect SELCET areaclass" data-placeholder="Select Area" id="areas" name="address_area[]">
                <option></option>
            </select>
        <div class="row custom_radio_class"><label for="area_name" class="error" style="display:none;"></label></div>
        </div>
    </div>
    <div class="form-group row">
        <label for="inputName2" class="col-sm-4 col-form-label ">PIN Code<span class="required_star">*</span></label>
        <div class="col-sm-8">
            <input type="text" class="form-control pin only-number" id="pin" placeholder="PIN" name="pin[]" value="">
        </div>
    </div>

    <div class="col-custom custom_radio_class ml-0 mt-0">
        <div class="icheck-primary d-inline">
            <input type="radio" id="" class="address_primary_radio" value="" name="address_primary" autocomplete="off" >
            <label for="" class="address_primary_class">Primary</label>
        </div>
    </div>
    {{-- <div class="col-custom custom_radio_class ml-0 mt-0">
        <div class="icheck-primary d-inline">
            <input type="radio" id="" class="address_active_radio" value="" name="address_active" autocomplete="off" >
            <label for="" class="address_active_class">Active</label>
        </div>
    </div> --}}
    {{-- <div class="text-right pr-3 col-md-6"><a href="javascript:void(0)"  class="btn btn-danger removeVariant d-inline-block" data-row="">Remove</a></div> --}}
</div>
