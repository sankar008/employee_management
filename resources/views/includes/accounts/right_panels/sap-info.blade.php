<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
<h6>SAP Details</h6>
</div>

<form class="form-horizontal sap_information_form" id="update-sap_information-form" action="javascript:void(0)">
    @php $flatten_products = $products->mapWithKeys(function($item){
        return [$item->product_id => $item->product_name];
    });

    @endphp
    <div class="row m-0" data-product="1">
        @php $count = count($details->get_account_sap); @endphp
        @if($count > 0)
        @for($i = 0; $i <= $count - 1; $i++)
        <div class="__sap_div_wrapper col-md-12 __rowId{{$i + 1}} row __SAP_ROW pt-3 pb-3 m-0 @if($i != 0) @endif" id="__saprowId{{$i + 1}}" data-saprowid="{{$i + 1}}">
            @if($i != 0) <div class="class-cross removeVariantSap"><span><i class="fas fa-times"></i></span></div> @endif
            <input type="hidden" class="__databaserowid" name="databaserowid[]" value="{{$details->get_account_sap[$i]['id']}}">
            <div class="w-100 ml-2 custom_radio_class">
                <span class="badge __badge_primary product_name_badge">{{$flatten_products[$details->get_account_sap[$i]['product_id']]}}</span>
                <div class="icheck-primary d-inline">
                    <input type="radio" id="sap_primary{{$i + 1}}" class="sap_radio" value="" @if($details->get_account_sap[$i]['is_active'] == 1) {{'checked'}} @endif name="sap_primary_{{$details->get_account_sap[$i]['product_id']}}" autocomplete="off" data-product="{{$details->get_account_sap[$i]['product_id']}}">
                    <label for="sap_primary{{$i + 1}}" class="sap_label">
                        <div class="sap_radio_error sap_primary_{{$details->get_account_sap[$i]['product_id']}} error d-none">
                            Select primary SAP for {{$flatten_products[$details->get_account_sap[$i]['product_id']]}}
                        </div>
                    </label>
                </div>
            </div>

            <div class="col-md-4">
                <div class="sap_details">
                    <label for="">Sold to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __soldto" placeholder="Sold to" name="soldto[]" value="{{$details->get_account_sap[$i]['sold_to']}}" maxlength="10" autocomplete="off">
                </div>
            </div>
            <div class="col-md-4">
                <div class="sap_details">
                    <label for="">Ship to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __shipto" placeholder="Ship to" name="shipto[]" value="{{$details->get_account_sap[$i]['ship_to']}}" maxlength="10" autocomplete="off">
                </div>
            </div>
            <div class="col-md-4">
                <div class="sap_details">
                    <label for="">Payer to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __payto" placeholder="Payee" name="payto[]" value="{{$details->get_account_sap[$i]['pay_to']}}" maxlength="10" autocomplete="off">
                </div>
            </div>
        </div>
        @endfor
        @else
        <div class="__sap_div_wrapper col-md-12 __rowId1 row __SAP_ROW pt-3 pb-3 m-0" id="__saprowId1" data-saprowid="1">
            <input type="hidden" class="__databaserowid" name="databaserowid[]" value="0">
            <div class="w-100 ml-2 custom_radio_class">
                <span class="badge __badge_primary product_name_badge">TMT</span>
                <div class="icheck-primary d-inline">
                    <input type="radio" id="sap_primary1" class="sap_active sap_radio" name="sap_primary_1" value="" data-product="1">
                    <label for="sap_primary1" class="sap_active_label">
                        <div class="sap_radio_error  sap_primary_1 error d-none">
                            Select primary SAP for TMT
                        </div>
                    </label>
                </div>
            </div>

            <div class="col-md-3">
                <div class="sap_details">
                    <label for="">Sold to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __soldto" placeholder="Sold to" name="soldto[]" value="" maxlength="10" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3">
                <div class="sap_details">
                    <label for="">Ship to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __shipto" placeholder="Sold to" name="shipto[]" value="" maxlength="10" autocomplete="off">
                </div>
            </div>
            <div class="col-md-3">
                <div class="sap_details">
                    <label for="">Payer to Party <span class="required">*</span></label>
                    <input type="text" class="form-control sapid only-number __payto" placeholder="Payee" name="payto[]" value="" maxlength="10" autocomplete="off">
                </div>
            </div>
        </div>
        @endif
    </div>
    @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true')
        <div class="blue-btn-wrap text-right col-md-12 d-flex">
            <input type="button" class="btn btn-success ml-auto mr-2" name="add_more_sap" id="add_more_sap" value="Add More" autocomplete="off">
            <div class="col-md-2 ml-0 d-none" id="__product_sap_selection">
                <select class="form-control select2bs4 products_dropdown" data-placeholder="Select product group" id="product_sap" name="product_sap" value="">
                  <option value=""></option>
                  @if (!$products->isEmpty())
                      @foreach ($products as $product)
                      <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                      @endforeach
                  @endif
                </select>
            </div>
            <input type="submit" class="btn btn-primary" id="sap_information" data-type="sap_information" value="Update">
        </div>
    @endif
</form>

<!-- template -->
<div class="__sap_div_wrapper col-md-12 row sap_clone hide pt-3 pb-3 m-0">
    <div class="class-cross removeVariantSap"><span><i class="fas fa-times"></i></span></div>
    <input type="hidden" class="__databaserowid" name="databaserowid[]" value="0">
    <div class="w-100 ml-2 custom_radio_class">
        <span class="badge __badge_primary product_name_badge"></span>
        <div class="icheck-primary d-inline">
            <input type="radio" id="" class="sap_active" value="">
            <label for="sap_primary" class="sap_active_label">
                <div class="sap_radio_error error d-none">

                </div>
            </label>
        </div>
    </div>

    <div class="col-md-4">
        <div class="sap_details">
            <label for="">Sold to Party <span class="required">*</span></label>
            <input type="text" class="form-control sapid only-number __soldto" placeholder="Sold to" name="soldto[]" value="" maxlength="10" autocomplete="off">
        </div>
    </div>
    <div class="col-md-4">
        <div class="sap_details">
            <label for="">Ship to Party <span class="required">*</span></label>
            <input type="text" class="form-control sapid only-number __shipto" placeholder="Sold to" name="shipto[]" value="" maxlength="10" autocomplete="off">
        </div>
    </div>
    <div class="col-md-4">
        <div class="sap_details">
            <label for="">Payer to Party <span class="required">*</span></label>
            <input type="text" class="form-control sapid only-number __payto" placeholder="Payee" name="payto[]" value="" maxlength="10" autocomplete="off">
        </div>
    </div>
</div>
<!-- template ends-->
