@if(Auth::user()->hasRole('super admin') || Auth::user()->hasRole('HO'))
<li class="list-group-item">
    <button class="btn btn-success calltoLead mb-2" id="__status_change" data-toggle="modal" data-target="#activation-modal">Change status</button>
</li>
@endif


<div class="modal fade show" id="activation-modal" aria-modal="true" style="padding-right: 17px;">
    <div class="modal-dialog modal-lg modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-header">
              <h4 class="modal-title">Change status</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
              </button>
          </div>
          <div class="modal-body">
              <form action="javascript:void(0)" id="account_currentstatus_form" class="account_currentstatus_form">
                 <div class="row">
                  <div class="col-md-12">
                    <div class="form-group row col-md-12">
                        <label for="inputEmail" class="col-sm-4 col-form-label">Account status <span class="required_star">*</span></label>
                        <div class="col-sm-8">
                            <select class="form-control select2bs4" data-placeholder="Select status" id="current_status" name="current_status">
                                <option></option>
                                <option value="5" @if($details->status == 5) {{'selected'}} @endif>Active</option>
                                <option value="7" @if($details->status == 7) {{'selected'}} @endif>Inactive</option>
                                <option value="6" @if($details->status == 6) {{'selected'}} @endif>Suspended</option>
                            </select>
                            <div class="row custom_radio_class"><label for="current_status" class="error" style="display:none;"></label></div>
                        </div>
                      </div>

                      <div class="form-group row col-md-12">
                            <label for="inputEmail" class="col-sm-4 col-form-label">Remark <span class="required_star">*</span></label>
                            <div class="col-sm-8">
                                <textarea name="account_status_remark" id="account_status_remark" class="form-control" placeholder="Type remark"></textarea>
                            </div>
                      </div>
                  </div>
                 </div>
                  <div class="blue-btn-wrap text-right">
                    <input type="submit" class="btn btn-primary data-update" data-type="account_currentstatus" value="Update status">
                  </div>
              </form>
          </div>
        </div>
    </div>
</div>
