<!-- sales tab -->
<div class="tab-pane" id="sales" style="height: 400px">
    <div id="dealerlifting-render-div">
        <form id="dealer-sales-tab-form" action="javascript:void(0)">
            <div class="row d-flex">
                <div class="col-sm-3">
                    <select class="form-control select2bs4" data-placeholder="Select product" id="sales_tab_products">
                        <option></option>
                        @if (!$products->isEmpty())
                            @foreach ($products as $product)
                            <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                            @endforeach
                        @endif
                      </select>
                </div>
                <div class="text-right pr-3 pb-3">
                    <input type="submit" class="btn btn-primary" id="search_sales_information" value="Search">
                </div>
            </div>
        </form>
        <div class="col-md-12 row mb-2">
            <div class="col-md-6">
                <p class="font-weight-bold font-italic text-success mt-0 mb-0"><sup>*</sup> All lifting quntities are in Metric tonnes (mt)</p>
            </div>
            <div class="col-md-6 text-right">
                <p class="font-weight-bold font-weight-bold font-italic ml-2 mt-0 mb-0 text-primary">Activation Date: @if(isset($details->created_at)) {{ formattedDateTime($details->created_at, 1) }} @else N/A @endif</p>
            </div>
        </div>
        <div class="row" id="__liftingappend_dealer">

        </div>
    </div>
</div>
