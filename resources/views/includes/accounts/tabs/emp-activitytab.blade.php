<div class="container">
    <div class="card">
        <div class="card-header">
            <form action="">
                <div class="row">
                    <div class="col-lg-4">
                        
                        <select id="amonth" name="month" class="form-control select2bs4" data-placeholder="Month">
                        </select>
                    </div>
                    <div class="col-lg-4">
                        
                        <select id="ayear" name="year" class="form-control select2bs4" data-placeholder="Year">
                            <option></option>
                                <option selected>{{date('Y')}}</option>
                                <option>{{date('Y')-1}}</option>
                                <option>{{date('Y')-2}}</option>
                        </select>
                    </div>
                    <div class="col-lg-4">
                        <input type="submit" id="act-btn" value="Search" class="btn btn-warning">
                    </div>
                </div>
            </form>
        </div>
        <div class="card-body">
            <!-- employee activity tab -->
            <div class="tab-pane" id="emp-activity">
                <div class="demo-container">
                    <div id="activity-report-summary-grid" style="height:600px"></div>
                </div>
            </div>
        </div>
    </div>

</div>


