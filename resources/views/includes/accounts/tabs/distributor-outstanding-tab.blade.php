<!-- distributor outstanding tab for summary page -->
<div class="tab-pane" id="outstanding">
    <form id="dealer-outstanding-tab-form" action="javascript:void(0)">
        <div class="row mb-2">
            <div class="col-md-4">
                {{-- <label for="inputName2" class="col-sm-2 col-form-label mb-2">Products<span class="required_star">*</span></label> --}}
                <select class="form-control select2bs4" data-placeholder="Select product" id="outstanding_product_id" name="product_id" data-bind-div="outstanding_dealer_id" data-bind-type="account_id">
                    <option></option>
                    @if (!$products->isEmpty())
                        @foreach ($products as $product)
                        <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="col-md-4">
                <select name="dealer_id" class="form-control dealers_list outstanding_dealer_id" id="outstanding_dealer_id" data-placeholder="Select dealer"><option></option></select>
            </div>

            <div class="col-md-2">
                <div class="text-left">
                <button type="submit" class="btn btn-primary" id="filter-outstanding">Search</button>
                <button type="submit" class="btn btn-success" id="clear-outstanding">Clear</button>
                </div>
            </div>
        </div>
    </form>

    <div class="form-group row">
        <div class="demo-container">
            <div id="outstanding_div" style="height:900px" class="hasCustomIcons"></div>
        </div>
    </div>
</div>
