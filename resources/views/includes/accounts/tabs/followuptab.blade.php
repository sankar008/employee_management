<!-- followup tab -->
<div class="tab-pane" id="followup">
    <div id="icon" class="col-md-12 pt-3 pl-0">
        <ul class="mb-2 ml-0">
            <li><b>Legend: </b></li>
            <li><div class="foo text-primary ml-1 mt-0 mr-1"><i class="fas fa-bus reply-to"></i></div><b>Visit</b></li>
            <li><div class="foo text-primary ml-1 mt-0 mr-1"><i class="fas fa-phone-alt text-primary"></i></div><b>Call</b></li>
            <li><div class="foo text-primary ml-1 mt-0 mr-1"><i class="fas fa-envelope"></i></div><b>Email</b></li>
            <li><div class="foo text-success ml-1 mt-0 mr-1"><i class="fas fa-users"></i></div><b>Grouptask</b></li>
        </ul>
    </div>
    <div class="demo-container">
        <div id="task-render-div" style="height:600px"></div>
        <div id="loadpanel"></div>
    </div>
</div>
