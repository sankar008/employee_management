<!-- ledger tab for distributor summary page -->
<div class="tab-pane" id="ledger">
    <form action="javascript:void(0)" id="dealer-ledger-tab-form">
        <div class="row mb-2">
            <div class="col-md-2">
                {{-- <label for="inputName2" class="col-sm-2 col-form-label mb-2">Products<span class="required_star">*</span></label> --}}
                <select class="form-control select2bs4 ledger_product_id" data-placeholder="Select product" id="ledger_product_id" name="product_id" data-bind-div="ledger_dealer_id" data-bind-type="sap_code">
                    <option></option>
                    @if (!$products->isEmpty())
                        @foreach ($products as $product)
                        <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                        @endforeach
                    @endif
                </select>
                <div class="row custom_radio_class"><label for="ledger_product_id" class="error" style="display:none;"></label></div>
            </div>

            <div class="col-md-4">
                <select name="dealer_id" class="form-control dealers_list ledger_dealer_id" id="ledger_dealer_id" data-placeholder="Select dealer">
                    <option></option>
                </select>
                <div class="row custom_radio_class"><label for="ledger_dealer_id" class="error" style="display:none;"></label></div>
            </div>

            <div class="col-md-4">
                <select name="financial_year" id="financial_year" data-placeholder="Select financial year" class="form-control select2bs4 financial_year">
                    <option></option>
                    @foreach (financialYears() as $year)
                    <option value="{{$year}}" @if(getCurrentFinancialYear() == $year) {{"selected"}} @endif>{{$year}} - {{$year + 1}}</option>
                    @endforeach
                </select>
                <div class="row custom_radio_class"><label for="financial_year" class="error" style="display:none;"></label></div>
            </div>

            <div class="col-md-2">
                <div class="text-left">
                    <button type="submit" class="btn btn-primary" id="filter-ledger">Search</button>
                </div>
            </div>
        </div>
    </form>

    <div class="form-group row d-flex">
      <div class="card-body col-md-12 card-body2 p-0">
        <div id="ledger-div" style="height:600px"></div>
      </div>
    </div>
</div>
