<!-- outstanding tab -->
<div class="tab-pane" id="outstanding">
    <form id="dealer-outstanding-tab-form" action="javascript:void(0)">
        <div class="row d-flex">
            <div class="col-sm-3">
                <select class="form-control select2bs4" name="product_id" data-placeholder="Select product" id="outstanding_tab_products">
                    <option></option>
                    @if (!$products->isEmpty())
                        @foreach ($products as $product)
                        <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                        @endforeach
                    @endif
                  </select>
            </div>
            <div class="text-right pr-3 pb-3">
                <input type="submit" class="btn btn-primary" id="search_outstanding_information" value="Search">
            </div>
        </div>
    </form>
    <div class="demo-container">
        <div id="outstanding-render-div" style="height:200px"></div>
    </div>
</div>
