<!-- user profile tab -->
<div class="tab-pane" id="user_profile">
    <div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
        <h6> User Profile</h6>
    </div>

    <form action="javascript:void(0)" class="form-horizontal update-account-form profile_information_form" id="edit-user-form">
        <div class="form-group row profile-form">
            <!-- report to dropdown -->
            <label for="exampleInputEmail1" class="col-md-4 col-form-label">Reporting Manager <span class="required_star">*</span></label>
            <div class="col-sm-8">
                <select class="form-control select2bs4" data-placeholder="Select employee" id="report_to" name="report_to" multiple >
                    <option></option>
                    @if(!$report_to->isEmpty())
                    @foreach($report_to as $eachreportingTo)
                        <option value="{{$eachreportingTo->id}}" @isset($details->tagged_report_to->emp_id_array)
                            @if(in_array($eachreportingTo->id, $details->tagged_report_to->emp_id_array)) selected @endif
                        @endisset>{{$eachreportingTo->name}}</option>
                    @endforeach
                    @else
                        <option value="0">No employee available</option>
                    @endif
                </select>
                <div class="row custom_radio_class"><label for="report_to" class="error" style="display:none;"></label></div>
            </div>
        </div>

        <!-- report from dropdown -->
        <div class="form-group row profile-form">
            <label for="exampleInputEmail1" class="col-md-4 col-form-label">Reporting from <span class="required_star">*</span></label>
            <div class="col-sm-8">
                <select class="form-control select2bs4" data-placeholder="Select employee" id="report_from" name="report_from" multiple>
                    <option></option>
                    @if(!$report_from->isEmpty())
                    @foreach($report_from as $eachreportingFrom)
                        <option value="{{$eachreportingFrom->id}}" @isset($details->tagged_report_from->emp_id_array)
                            @if(in_array($eachreportingFrom->id, $details->tagged_report_from->emp_id_array)) selected @endif
                        @endisset>{{ $eachreportingFrom->name }} @isset($eachreportingFrom->get_user_by_profile->profile_name) ({{ ucwords($eachreportingFrom->get_user_by_profile->profile_name) }})  @endisset</option>
                    @endforeach
                    @else
                        <option value="0">No employee available</option>
                    @endif
                </select>
            </div>
        </div>

        <div class="form-group row">
            <label for="exampleInputEmail1" class="col-sm-4 col-form-label">User Name</label>
            <div class="col-sm-8 mb-2">
                <input type="text" name="" id="username__" class="form-control" readonly value="{{ $user_name }}">
            </div>
        </div>

        <div class="form-group row">
            <label for="exampleInputEmail1" class="col-sm-4 col-form-label">Last Password</label>
            <div class="col-sm-8 mb-2">
                <input type="text" name="" id="password__" class="form-control" readonly value="{{ $last_password  }}">
            </div>
        </div>

        @if($ACCESS_RIGHTS['CAN_EDIT'] === 'true' && Auth::user()->id == 1)
        <div class="blue-btn-wrap text-right" style="float:right;">
            <input type="submit" class="btn btn-primary data-update"  data-type="profile_information"  value="Update">
        </div>
        @endif
    </form>
</div>
