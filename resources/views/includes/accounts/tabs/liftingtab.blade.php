<!-- lifting tab -->
<div class="tab-pane" id="lifting" style="height: auto">
    <form action="javascript:void(0)" id="subdealer-lifting-tab-form">
        <div class="col-md-12 row mb-2">
            <div class="col-md-2 pb-1">
                <select name="financial_year" id="financial_year" data-placeholder="Select financial year"
                    class="form-control select2bs4">
                    <option></option>
                    @foreach (financialYears() as $year)
                        <option value="{{ $year }}"
                            @if (getCurrentFinancialYear() == $year) {{ 'selected' }} @endif>{{ $year }} -
                            {{ $year + 1 }}</option>
                    @endforeach
                </select>
                <div class="row custom_radio_class"><label for="financial_year" class="error"
                        style="display:none;"></label></div>
            </div>
            <div class="col-md-2">
                <input type="submit" class="btn btn-primary search" value="Search" id="filter-lifting"/>
            </div>
        </div>
    </form>

    <div class="col-md-12 row mb-2">
        <div class="col-md-6">
            <p class="font-weight-bold font-italic text-success mt-0 mb-0"><sup>*</sup> All lifting quntities are in
                Metric tonnes (mt)</p>
        </div>
        <div class="col-md-6 text-right">
            <p class="font-weight-bold font-weight-bold font-italic ml-2 mt-0 mb-0 text-primary">Activation Date:
                @if (isset($details->created_at))
                    {{ formattedDateTime($details->created_at, 1) }}
                @else
                    N/A
                @endif
            </p>
        </div>
    </div>
    <div id="lifting-render-div">
        <div class="row" id="__liftingappend">

        </div>
    </div>
</div>
