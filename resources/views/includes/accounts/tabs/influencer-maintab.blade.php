<div class="card-header info-tab-header scrollable-tab">
    <button id="left-arw"><i class="fa fa-chevron-left" aria-hidden="true"></i></button>
    <ul class="nav nav-pills">
      <li class="nav-item"><a class="nav-link tab-class active" href="#details" data-toggle="tab">Details</a></li>
      @isset($AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id'])
      @if(in_array('activity',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
        <li class="nav-item"><a class="nav-link tab-class" href="#activity" data-toggle="tab">Activity</a></li>
      @endif
      @if(in_array('followup',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
        <li class="nav-item"><a class="nav-link tab-class" href="#followup" data-toggle="tab">Followup</a></li>
      @endif
      @if(in_array('visit',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
      <li class="nav-item"><a class="nav-link tab-class" href="#visit" data-toggle="tab">Visit</a></li>
      @endif
      @if(in_array('location',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
      <li class="nav-item"><a class="nav-link tab-class" href="#location" data-toggle="tab" data-latitude="@if(!empty($locationdata)){{$locationdata->lat}}@endif" data-longitude="@if(!empty($locationdata)){{$locationdata->long}}@endif">Location</a></li>
      @endif
      @if(in_array('ihb_project',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
        <li class="nav-item"><a class="nav-link tab-class-account-tagged" href="#ihb_project" id="ihb_project_tagged_show" data-toggle="tab">IHB Projects</a></li>
      @endif
      @if(in_array('scheme',$AVAILABLE_PAGE_ITEMS['user_assigned_tabs']['page_id']))
        <li class="nav-item"><a class="nav-link" href="#scheme" data-toggle="tab">Scheme</a></li>
      @endif
      @endisset
    </ul>
    <button id="right-arw"><i class="fa fa-chevron-right" aria-hidden="true"></i></button>
</div>
