<!-- distributor lifting tab for summary page -->
<div class="tab-pane" id="lifting">
    <form id="dealer-sales-tab-form" action="javascript:void(0)">
        <div class="row d-flex">
            <div class="col-sm-4">
                <select class="form-control select2bs4" data-placeholder="Select product" id="sales_tab_products" name="dispatch_product_id">
                    <option></option>
                    @if (!$products->isEmpty())
                        @foreach ($products as $product)
                        <option value="{{$product->product_id}}">{{$product->product_name}}</option>
                        @endforeach
                    @endif
                </select>
            </div>

            <div class="col-sm-4">
                <select name="lifting_month" class="select2bs4 form-control lifting_month" data-placeholder="Select month">
                  <option></option>
                  @for($m = 1; $m <= 12; ++$m)
                    <option value="{{ $m }}" @if($m == date('m')){{"selected"}}@endif>{{date('F', mktime(0, 0, 0, $m, 1))}}</option>
                  @endfor
                </select>
            </div>

            <div class="text-right pr-3 pb-3">
                <input type="submit" class="btn btn-primary" id="search_sales_information" value="Search">
            </div>
        </div>
    </form>

    <div class="form-group row">
      <div class="card-body col-md-12 card-body2 p-0">
        <div id="lifting-div" style="height:600px"></div>
      </div>
    </div>
</div>
