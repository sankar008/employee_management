<script>
    /* dropzone js */
    var TODAY = '<?php echo date('d-m-Y');?>';

    if($("#kyc_files").length > 0) {
        Dropzone.autoDiscover = false;
        var myDropzone = new Dropzone(
                                        "div#kyc_files",
                                        {
                                            url: '{{route("upload-temp")}}',
                                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                                            maxFiles: 4,
                                            addRemoveLinks: true,
                                            autoProcessQueue: false,
                                            acceptedFiles: 'image/*,application/pdf',
                                            parallelUploads: 10
                                        }
                                    );

        myDropzone.on("complete", function(file) {
            myDropzone.getQueuedFiles(file)
        });

        myDropzone.on('sending', function(file, xhr, formData){
            var doc_type = $('#doc-type').val();
            formData.append('account_id', account_id);
            formData.append('account_type', account_type);
            formData.append('doc_type', doc_type);
            formData.append('meta', $('#'+doc_type+'_no').val());
    });
    }
    $('input[name="account_tagging_type"]').click(function(){
        //do stuff
        if($(this).val() == 1)
        {
            $("#subdealer_show").hide();
            $("#dealer_show").show();
        }
        else{
            $("#subdealer_show").show();
            $("#dealer_show").hide();
        }
    });
    /* timedate picker */
    $('.task_date,.datepicker').daterangepicker({
      timePicker: false,
      drops: 'down', //position
      singleDatePicker: true, // enables single month
      minDate: today,  //disables previous date
      //maxDate: "+1Y",
      locale: {
          format: 'DD-MM-YYYY'
      },
      autoUpdateInput: true
    });

    //identify tab and call modal accordingly
    $(document).on('click','.modal_call',function(){
      if($('.has-Class').hasClass('active')){
        var href = $('.has-Class.active').attr('href');
        if(href == '#employee_followup'){
            $('#employee-tab').addClass('shown_tab');
            $('#add-followup-type').val('emp');
            $("#resp_emp_show").show();
            $("#selftask_assign").val('');
            $("#tasktitle").html('Add Employee Task');
        }else if(href == '#self_followup'){
            $('#employee-tab').removeClass('shown_tab');
            $('#add-followup-type').val('self');
            $("#resp_emp_show").hide();
            $("#selftask_assign").val(1);
            $("#tasktitle").html('Add Self Task');
        }
        $('#modal-self').modal('show');
      };
    });

    /* render self followup table */
    $('#table-followup-selfs, #table-followup-emp').DataTable().destroy();

    setTimeout(() => {
        render_self_followup();
       // render_emp_followups();
    }, 2000);


    function render_self_followup(){
      var type   = 'self';
      var is_api = false;
      var dataTable =  $('#table-followup-selfs').DataTable({
                          processing: true,
                          serverSide: true,
                          stateSave: true,
                          searching: false,
                          bLengthChange: false,
                          autoWidth: true,
                          ajax: {
                            url: '{{route("fetch-accounttasklist")}}',
                            type: 'GET',
                            headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        'Authorization': 'Bearer '+ BEARER
                                    },
                            data: {account_id, type, TYPE, account_type},
                          },
                          columns: [
                              { data: 'task_date'},
                              { data: 'task_title'},
                              { data: 'priority'},
                              { data: 'task_type'},
                              { data: 'task_description'},
                              { data: 'status'},
                              { data: 'action'}
                          ]
        });

        $('#table-followup-selfs tbody').on('click', '.details-control', function(){
            var dataid = $(this).attr('data-id');
            if($(this).find('.toggle' + dataid).hasClass('fa-sort-down')){
                $('.toggle' + dataid).removeClass('fa-sort-down');
                $('.toggle' + dataid).addClass('fa-sort-up color-down');
            }else{
                $('.toggle' + dataid).removeClass('fa-sort-up color-down');
                $('.toggle' + dataid).addClass('fa-sort-down');
            }
            var tr = $(this).closest('tr');
            var row = dataTable.row( tr );
            if(row.child.isShown()){
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                var id = $(this).attr('data-id');
                $('#activity_id').val(id);
                format(row.child);
                tr.addClass('shown');
                $('.shown').closest('tr').next('td').addClass('NEXT_TR');
            }
        });
    }

    /* render employee followup table */
    function render_emp_followups(){
      var type   = 'employee';
      var is_api = false;
      var dataTable =  $('#table-followup-emp').DataTable({
                          processing: true,
                          serverSide: true,
                          stateSave: true,
                          searching: false,
                          bLengthChange: false,
                          autoWidth: true,
                          ajax: {
                            url: '{{route("fetch-accounttasklist")}}',
                            type: 'GET',
                            headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        'Authorization': 'Bearer '+ BEARER
                                    },
                            data: {account_id, type, TYPE, account_type},
                          },
                          columns: [
                              { data: 'task_date'},
                              { data: 'task_title'},
                              { data: 'priority'},
                              { data: 'task_type'},
                              { data: 'task_description'},
                              { data: 'status'},
                              { data: 'action'}
                          ]
        });

        $('#table-followup-emp tbody').on('click', '.details-control', function(){
            var dataid = $(this).attr('data-id');
            if($(this).find('.toggle' + dataid).hasClass('fa-sort-down')){
                $('.toggle' + dataid).removeClass('fa-sort-down');
                $('.toggle' + dataid).addClass('fa-sort-up color-down');
            }else{
                $('.toggle' + dataid).removeClass('fa-sort-up color-down');
                $('.toggle' + dataid).addClass('fa-sort-down');
            }
            var tr = $(this).closest('tr');
            var row = dataTable.row( tr );
            if(row.child.isShown()){
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            } else {
                var id = $(this).attr('data-id');
                $('#activity_id').val(id);
                format(row.child);
                tr.addClass('shown');
                $('.shown').closest('tr').next('td').addClass('NEXT_TR');
            }
        });
    }

            myDropzone.on("complete", function(file) {
                myDropzone.getQueuedFiles(file)
            });

            myDropzone.on('sending', function(file, xhr, formData){
                var doc_type = $('#doc-type').val();
                formData.append('account_id', account_id);
                formData.append('account_type', account_type);
                formData.append('doc_type', doc_type);
                formData.append('meta', $('#'+doc_type+'_no').val());
            });

            $('input[name="account_tagging_type"]').click(function(){
                //do stuff
                if($(this).val() == 1)
                {
                    $("#subdealer_show").hide();
                    $("#dealer_show").show();
                }
                else{
                    $("#subdealer_show").show();
                    $("#dealer_show").hide();
                }
            });
            /* timedate picker */
            $('.task_date,.datepicker').daterangepicker({
            timePicker: false,
            drops: 'down', //position
            singleDatePicker: true, // enables single month
            minDate: TODAY,  //disables previous date
            //maxDate: "+1Y",
            locale: {
                format: 'DD-MM-YYYY'
            },
            autoUpdateInput: true
            });

            //identify tab and call modal accordingly
            $(document).on('click','.modal_call',function(){
            if($('.has-Class').hasClass('active')){
                var href = $('.has-Class.active').attr('href');
                if(href == '#employee_followup'){
                    $('#employee-tab').addClass('shown_tab');
                    $('#add-followup-type').val('emp');
                    $("#resp_emp_show").show();
                    $("#selftask_assign").val('');
                    $("#tasktitle").html('Add Employee Task');
                }else if(href == '#self_followup'){
                    $('#employee-tab').removeClass('shown_tab');
                    $('#add-followup-type').val('self');
                    $("#resp_emp_show").hide();
                    $("#selftask_assign").val(1);
                    $("#tasktitle").html('Add Self Task');
                }
                $('#modal-self').modal('show');
            };
            });

            /* render self followup table */
            $('#table-followup-selfs, #table-followup-emp').DataTable().destroy();

            setTimeout(() => {
                render_self_followup();
            // render_emp_followups();
            }, 2000);


            function render_self_followup(){
            var type   = 'self';
            var is_api = false;
            var dataTable =  $('#table-followup-selfs').DataTable({
                                processing: true,
                                serverSide: true,
                                stateSave: true,
                                searching: false,
                                bLengthChange: false,
                                autoWidth: true,
                                ajax: {
                                    url: '{{route("fetch-accounttasklist")}}',
                                    type: 'GET',
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                                'Authorization': 'Bearer '+ BEARER
                                            },
                                    data: {account_id, type, TYPE, account_type},
                                },
                                columns: [
                                    { data: 'task_date'},
                                    { data: 'task_title'},
                                    { data: 'priority'},
                                    { data: 'task_type'},
                                    { data: 'task_description'},
                                    { data: 'status'},
                                    { data: 'action'}
                                ],
                                drawCallback: function() {
                                            tooltip()
                                }
                });

                $('#table-followup-selfs tbody').on('click', '.details-control', function(){
                    var dataid = $(this).attr('data-id');
                    if($(this).find('.toggle' + dataid).hasClass('fa-sort-down')){
                        $('.toggle' + dataid).removeClass('fa-sort-down');
                        $('.toggle' + dataid).addClass('fa-sort-up color-down');
                    }else{
                        $('.toggle' + dataid).removeClass('fa-sort-up color-down');
                        $('.toggle' + dataid).addClass('fa-sort-down');
                    }
                    var tr = $(this).closest('tr');
                    var row = dataTable.row( tr );
                    if(row.child.isShown()){
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        var id = $(this).attr('data-id');
                        $('#activity_id').val(id);
                        format(row.child);
                        tr.addClass('shown');
                        $('.shown').closest('tr').next('td').addClass('NEXT_TR');
                    }
                });
            }

            /* render employee followup table */
            function render_emp_followups(){
            var type   = 'employee';
            var is_api = false;
            var dataTable =  $('#table-followup-emp').DataTable({
                                processing: true,
                                serverSide: true,
                                stateSave: true,
                                searching: false,
                                bLengthChange: false,
                                autoWidth: true,
                                ajax: {
                                    url: '{{route("fetch-accounttasklist")}}',
                                    type: 'GET',
                                    headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                                'Authorization': 'Bearer '+ BEARER
                                            },
                                    data: {account_id, type, TYPE, account_type},
                                },
                                columns: [
                                    { data: 'task_date'},
                                    { data: 'task_title'},
                                    { data: 'priority'},
                                    { data: 'task_type'},
                                    { data: 'task_description'},
                                    { data: 'status'},
                                    { data: 'action'}
                                ],
                                drawCallback: function() {
                                            tooltip()
                                    }
                });

                $('#table-followup-emp tbody').on('click', '.details-control', function(){
                    var dataid = $(this).attr('data-id');
                    if($(this).find('.toggle' + dataid).hasClass('fa-sort-down')){
                        $('.toggle' + dataid).removeClass('fa-sort-down');
                        $('.toggle' + dataid).addClass('fa-sort-up color-down');
                    }else{
                        $('.toggle' + dataid).removeClass('fa-sort-up color-down');
                        $('.toggle' + dataid).addClass('fa-sort-down');
                    }
                    var tr = $(this).closest('tr');
                    var row = dataTable.row( tr );
                    if(row.child.isShown()){
                        // This row is already open - close it
                        row.child.hide();
                        tr.removeClass('shown');
                    } else {
                        var id = $(this).attr('data-id');
                        $('#activity_id').val(id);
                        format(row.child);
                        tr.addClass('shown');
                        $('.shown').closest('tr').next('td').addClass('NEXT_TR');
                    }
                });
            }



            /* validdation for add task form */
            $("#add-task-lead-form").validate({
                rules:{
                    resp_emp_id: {
                    required: function(element) {
                            return $('#selftask_assign').val() == ''
                            }
                    },
                    task_type_id: {
                        required: true
                    },
                    task_date: {
                        required: true
                    },
                    task_title: {
                        required: true
                    },
                    task_description: {
                        required: true
                    },
                    task_priority_id: {
                        required: true
                    }
                },
                messages: {
                    resp_emp_id: {
                        required: 'Please select employee'
                    },
                    task_type_id: {
                        required: 'Please select task type list'
                    },
                    task_date: {
                        required: 'Please enter task date'
                    },
                    task_title: {
                        required: 'Please enter task subject'
                    },
                    task_description: {
                        required:'Please enter Remarks'
                    },
                    task_priority_id: {
                        required:'Please select task priority'
                    }
                },
                submitHandler: function(form){
                    var submitbutton = $(this.submitButton).attr('data-pid');
                    var data = $('#add-task-lead-form').serializeArray();
                    dataObj  = {};
                    $(data).each(function(i, field){
                        dataObj[field.name] = field.value;
                    });
                    var dataArray = {
                        'userdata' : dataObj,
                    };
                    var data = JSON.stringify(dataArray);
                    $.ajax({
                        url: '{{route("submit-account-task")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    'Authorization': 'Bearer '+ BEARER
                        },
                        data: {data,account_type,account_id},
                        complete: function(payload){
                            var status  = payload.status;
                            var newData = payload.responseJSON;
                            if(status == 200){
                                if(newData.message == 'duplicate task'){
                                    toastr.warning('Task already exist on same day');
                                }else if(newData.message == 'success'){
                                    toastr.success('Task added successfully');
                                }
                            }else{
                                toastr.error('Something went wrong please try again later')
                            }

                            if(submitbutton == 1){
                                $('#modal-self').modal('hide');
                            }
                            //reset form
                            $('#add-task-lead-form input').removeClass('error');
                            $('#add-task-lead-form select').removeClass('error');
                            $('#add-task-lead-form input').trigger("reset");
                            $('#add-task-lead-form').trigger("reset");
                            $(".task_dropdown").val([]).trigger('change');
                            $('#task_date').val(TODAY);
                            if($('#selftask_assign').val() == 1){
                                $('#table-followup-selfs').DataTable().destroy();
                                render_self_followup();
                            }else{
                                $('#table-followup-emp').DataTable().destroy();
                                render_emp_followups();
                            }
                        }
                    });
                },
            });

            /* validation for reamrks form */
            $("#add-reamrks-form").validate({
            rules:{
                remarkT: {
                required: true
                },
                rescheduled_date: {
                            required: function(element) {
                            return $('#rescheduled').is(':checked') == true
                            }
                }
            },
            messages: {
                remarkT: {
                required: 'Please type your remark'
                },
                rescheduled_date: {
                required: 'Please select date'
                }
            },
            submitHandler: function() {
                var id     = $('#activity_id').val();
                var remark = $('textarea#remarkT').val();
                var isRescheduled = $('#rescheduled').prop("checked");
                var date   = $('#rescheduled_date').val();
                var fType  = $('#fType').val();
                var account_confirm = $('#account_confirm').prop("checked");
                $.ajax({
                        url: '{{route("remarks-add-account")}}',
                        type: 'POST',
                        headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                'Authorization': 'Bearer '+ BEARER
                        },
                        data: {id, remark, isRescheduled, date, TYPE, fType,account_confirm},
                        success: function(data){

                        },
                        complete: function(payload, xhr, settings){
                        //console.log(payload.responseJSON);
                        $('#modal-remarks').modal('hide');
                        if(payload.status == 200){
                            if(isRescheduled == false){
                                toastr.success('Remarks added');
                            }else{
                                toastr.success('Task rescheduled');
                            }
                        }else if(payload.status == 403){
                            toastr.error('You can\'t rescheduled this task');
                        }else{
                            toastr.error('Something went wrong, Please try again later.');
                        }

                        $('#table-followup-selfs').DataTable().destroy();
                        render_self_followup();
                        $('#table-followup-emp').DataTable().destroy();
                        render_emp_followups();
                        format();
                        }
                });
            }
            });


            //append child table if any available callback method
            function format(callback){
                var followup_id = $('#activity_id').val();
                $.ajax({
                url: '{{route("fetch-child-task",0)}}',
                type: 'GET',
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            'Authorization': 'Bearer '+ BEARER
                            },
                data: {followup_id},
                success: function (data) {
                    var newData = JSON.parse(data);
                    var DATA = newData.data;
                    if(DATA.length > 0){
                    var html = '<thead><tr><th>Reschudele Date</th><th>Reason</th><th>Status</th></tr></thead>';
                    $.each(DATA, function(i,Data){
                        html += '<tr><td>'+Data.followup_date+'</td><td>'+Data.remarks+'</td><td>'+Data.status+'</td></tr>';
                    });
                    callback($('<table id="childtable" border="0" style="padding-left:50px;width:100%">'+html+'</table>')).show();
                    }
                }
            });
            }


            /* uplaod modal */
            $('.document-upload').on('click',function(){
                var doc_type = $(this).attr('data-type');
                $('#doc-type').val(doc_type);
                $('#upload-modal').modal('show');
            });

            /* upload area */
            $('.upload-file').on('click',function(){
                var files = myDropzone.files;
                if(files.length > 0){
                    myDropzone.processQueue();
                }
            });



            function primary_sales_info(){
                var dataTable =  $('#dispatch_item_show-list').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        //'Authorization': 'Bearer '+ BEARER
                                },
                            url: '{{route("get-primary-sales")}}',
                            data: {account_id,account_type}
                        },
                        columns: [
                            { data: 'invoice_no'},
                            { data: 'ship_id'},
                            { data: 'quantity'}
                        ]
                    });
            }


            function secondary_sales_info(){
                var dataTable =  $('#secondary_item_show-list').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        //'Authorization': 'Bearer '+ BEARER
                                },
                            url: '{{route("get-secondary-sales")}}',
                            data: {account_id,account_type}
                        },
                        columns: [
                            { data: 'invoice_no'},
                            { data: 'ship_id'},
                            { data: 'quantity'}
                        ]
                    });
            }


            function ihb_call_details_fn(){
                var i = 1;
                var dataTable =  $('#ihb_call_details-list').DataTable({
                        processing: true,
                        serverSide: true,
                        ajax: {
                            headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        //'Authorization': 'Bearer '+ BEARER
                                },
                            type: 'POST',
                            url: '{{route("get-account-callactivity-list")}}',
                            data: {account_id,account_type}
                        },
                        columns: [
                            { "render": function(data, type, full, meta) {
                                        return i++;
                                    }
                                },
                            { data: 'stage_of_construction'},
                            { data: 'stage_of_work'},
                            { data: 'requirement_val'},
                            { data: 'plot_area'},
                            { data: 'sentiment'},
                            { data: 'sentiment_action'},
                            { data: 'ihb_remarks'},
                            { data: 'name'},
                            {   data: 'call_time',
                                    type: 'num',
                                    render: {
                                        _: 'display',
                                        sort: 'timestamp'
                                    }
                                },
                            { data: 'call_duration'}
                        ]
                    });
            }


            /* tab render */
            $('.tab-class').on('click',function(){
                var url = $(this).attr('href');
                /* an loader */
                if(url == '#sales_details' || url == '#primary-item-show'){
                    $('#dispatch_item_show-list').DataTable().destroy();
                    primary_sales_info();
                }else if(url == '#secondary_item_show'){
                    $('#secondary_item_show-list').DataTable().destroy();
                    secondary_sales_info();
                }else if(url == '#ihb_call_details'){
                    $('#ihb_call_details-list').DataTable().destroy();
                    ihb_call_details_fn();
                }else if(url == '#ledger'){
                    @if(Route::current()->getName() == 'account.distributor.info')
                    $.ajax({
                        url: '{{route("accounts.getChild")}}',
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),'Authorization': 'Bearer '+ BEARER, 'MobileApp' : 'true', 'Origin-Type' : 'web'},
                        data: {requested_account_type: 'dealer', parent_account_id: account_id},
                        complete: function(payload, xhr, settings){
                            var data = payload.responseJSON.data
                            if(payload.status == 200){
                                var html = ''
                                $('.dealers_list').find('option').not(':first').remove()
                                $.each(data, function(i){
                                    var html = '<option value="'+data[i].sap_code+'" data-sap="'+data[i].sap_code+'" data-id="'+data[i].dealer_id+'">'+ data[i].name +'</option>'
                                    $('.dealers_list').append(html)
                                })
                            }
                        }
                    })
                    @endif

                    //set placeholder in dealer list
                    $('.dealers_list').one('select2:open', function(e) {
                        $('input.select2-search__field').prop('placeholder', 'Search by dealer name or sap id')
                    })

                    /**
                     * filter
                     */
                    $('#filter-ledger').on('click', function(){
                        var sap_code = $('.ledger_dealer_id').val()
                        var financial_year = $('#financial_year').val()
                        if(financial_year == '' || sap_code == ''){
                            toastr.error('Please select dealer and financial year')
                        }else{
                            fetchLedger(account_id, sap_code, financial_year)
                        }
                    })

                    //fetchOutstanding(account_id, sap_code = '')

                    function fetchLedger(account_id, sap_code = '', financial_year = ''){
                        $('.preloader2').css('display','block')
                        var url = '{{route("ledger.getData")}}'
                        $.ajax({
                            url: url,
                            type: 'GET',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),'Authorization': 'Bearer '+ BEARER, 'MobileApp' : 'true', 'Origin-Type' : 'web'},
                            data: {account_id, sap_code, financial_year},
                            complete: function(payload, xhr, settings){
                                var LedgerArray = payload.responseJSON.data.currentFY
                                var previousFY = payload.responseJSON.data.previousFY
                                var currentFYC = payload.responseJSON.data.currentFYC
                                LedgerArray.unshift(previousFY) //push to 0 index
                                LedgerArray.push(currentFYC) // push to last index
                                Ledger(LedgerArray)
                            }
                        })
                    }


                    function Ledger(data){
                        $("#ledger-div").dxDataGrid({
                            dataSource: data,
                            showBorders: true,
                            selection: {
                                mode: "single"
                            },
                            scrolling: {
                                mode: "virtual"
                            },
                            sorting: {
                                mode: "none"
                            },
                            rowAlternationEnabled: true,
                            columns: [
                                {
                                    dataField: "particulars",
                                    width: 280,
                                    caption: "Particulars"
                                }, {
                                    dataField: "doc_no",
                                    caption: "Doc no"
                                }, {
                                    dataField: "post_date",
                                    dataType: "date",
                                    format: { skeleton: "d" },
                                    caption: "Date"
                                }, {
                                    dataField: "debit_amount",
                                    caption: "Debit amount"
                                }, {
                                    dataField: "credit_amount",
                                    caption: "Credit amount"
                                },
                                {
                                    dataField: "outstanding",
                                    caption: "Outstanding"
                                }
                            ]
                        });
                        $('.preloader2').css('display','none')
                    }

                }else if(url == '#lifting'){
                    $('.preloader2').css('display','block')
                    @if(Route::current()->getName() == 'account.distributor.info')

                    $('.lifting_month').on('change', function(){
                        var month = $(this).val()
                        $('.preloader2').css('display','block')
                        getLiftingData(sap_code = '',month,account_id)
                    })

                    getLiftingData(sap_code = '',month = '',account_id)
                    function getLiftingData(sap_code,month,account_id){
                        var url = '{{route("lifting.getData")}}'
                        $.ajax({
                            url: url,
                            type: 'GET',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),'Authorization': 'Bearer '+ BEARER, 'MobileApp' : 'true', 'Origin-Type' : 'web'},
                            data: {sap_code, month, account_id},
                            complete: function(payload, xhr, settings){
                                var data = payload.responseJSON.data
                                var response = []
                                var responseObj = {}
                                $.each(data, function(i){
                                    var current_month_lifting = data[i].current_month_lifting
                                    var last_month_lifting = data[i].last_month_lifting
                                    var last_year_lifting = data[i].last_year_lifting
                                    var currentMonthLifting = 0
                                    var lastMonthLifting = 0
                                    var latYearthisMonth = 0

                                    if(current_month_lifting.length > 0){
                                        $.each(current_month_lifting, function(i){
                                            currentMonthLifting += current_month_lifting[i].quantity
                                        })
                                        currentMonthLifting = parseFloat(currentMonthLifting).toFixed(2)
                                    }

                                    if(last_month_lifting.length > 0){
                                        $.each(last_month_lifting, function(i){
                                            lastMonthLifting += parseFloat(last_month_lifting[i].quantity)
                                        })
                                        lastMonthLifting = parseFloat(lastMonthLifting).toFixed(2)
                                    }

                                    if(last_year_lifting.length > 0){
                                        $.each(last_year_lifting, function(i){
                                            latYearthisMonth += parseFloat(last_year_lifting[i].quantity)
                                        })
                                        latYearthisMonth = parseFloat(latYearthisMonth).toFixed(2)
                                    }
                                    var responseObj = {
                                        dealer_name: data[i].name,
                                        sap_code: data[i].sap_code,
                                        currentMonthLifting: currentMonthLifting,
                                        lastMonthLifting: lastMonthLifting,
                                        latYearthisMonth: latYearthisMonth,
                                    }
                                    response.push(responseObj)
                                })

                                renderLiftingData(response)
                            }
                        })
                    }

                    function getCalenderData(M,Y){
                        var d = new Date()
                        var y = d.getFullYear()
                        var n = M
                        var y = y - Y
                        if(n == 1){m = 'Jan'}else if(n == 2){m = 'Feb'}else if(n == 3){m = 'Mar'}else if(n == 4){m = 'Apr'}
                        else if(n == 5){m = 'May'}else if(n == 6){m = 'Jun'}else if(n == 7){m = 'Jul'}
                        else if(n == 8){m = 'Aug'}else if(n == 9){m = 'Sep'}
                        else if(n == 10){m = 'Oct'}else if(n == 11){m = 'Nov'}else if(n == 12){m = 'Dec'}
                        return m +', '+y
                    }

                    function renderLiftingData(data){

                        var d = new Date()
                        var currentMonth = d.getMonth()
                        n = parseInt($('.lifting_month').val())

                        if(n == 1){
                            L = 12
                            Y = 1
                        }else{
                            L = n - 1
                            Y = 0
                        }

                        $("#lifting-div").dxDataGrid({
                            dataSource: data,
                            showBorders: true,
                            paging: {
                                enabled: false
                            },
                            scrolling: {
                                mode: "virtual"
                            },
                            rowAlternationEnabled: true,
                            columns: [
                                {
                                    dataField: "dealer_name",
                                    width: 280,
                                    caption: "Dealer name"
                                }, {
                                    dataField: "sap_code",
                                    width: 100,
                                    caption: "Sap ID"
                                }, {
                                    dataField: "currentMonthLifting",
                                    caption: getCalenderData(n,0)
                                }, {
                                    dataField: "lastMonthLifting",
                                    caption: getCalenderData(L,Y)
                                }, {
                                    dataField: "latYearthisMonth",
                                    caption: getCalenderData(n,1)
                                }
                            ],
                            summary: {
                                    totalItems: [
                                    {
                                        column: "currentMonthLifting",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "lastMonthLifting",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "latYearthisMonth",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    }
                                ]
                            }
                        })
                        $('.preloader2').css('display','none')
                    }

                    @endif
                }else if(url == '#outstanding'){
                    $('.preloader2').css('display','block')
                    @if(Route::current()->getName() == 'account.distributor.info')
                        $.ajax({
                            url: '{{route("accounts.getChild")}}',
                            type: 'GET',
                            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),'Authorization': 'Bearer '+ BEARER, 'MobileApp' : 'true', 'Origin-Type' : 'web'},
                            data: {requested_account_type: 'dealer', parent_account_id: 'Request::route("distributor_id")'},
                            complete: function(payload, xhr, settings){
                                if(payload.status == 200){
                                    var data = payload.responseJSON.data
                                    $('.outstanding_dealer_id').find('option').not(':first').remove()
                                    var html = ''
                                    $.each(data, function(i){
                                        var html = '<option value="'+data[i].sap_code+'" data-id="'+data[i].account_id+'">'+ data[i].name +'</option>'
                                        $('.outstanding_dealer_id').append(html)
                                    })
                                }
                            }
                        })
                        var type = 1;
                        getOutstandingData(account_id, sap_code = '')

                        function getOutstandingData(account_id, sap_code = ''){
                            var url = '{{route("outstanding.getData")}}'
                            $.ajax({
                                url: url,
                                type: 'GET',
                                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),'Authorization': 'Bearer '+ BEARER, 'MobileApp' : 'true', 'Origin-Type' : 'web'},
                                data: {account_id, sap_code},
                                complete: function(payload, xhr, settings){
                                    if(payload.status == 200){
                                        var data = payload.responseJSON.data
                                        var OutStandingArray = []
                                        $.each(data, function(i){
                                            var dealer_name = data[i].name
                                            var sap_code = data[i].sap_code
                                            var as_on_date = 'N/A'
                                            var net_amount = 'N/A'
                                            var lessthan31days = 'N/A'
                                            var thirty1to45days = 'N/A'
                                            var forty6to60days = 'N/A'
                                            var sixty1to90days = 'N/A'
                                            var ninety1to120days = 'N/A'
                                            var one21to9999days = 'N/A'
                                            if(data[i].outstanding_data != null){
                                                var as_on_date = data[i].outstanding_data.as_on_date
                                                var date = as_on_date.split('-')
                                                var date = date[2] +'-'+date[1] +'-'+date[0]
                                                var net_amount = parseFloat(data[i].outstanding_data.net_amount).toFixed(2)
                                                var lessthan31days = parseFloat(data[i].outstanding_data.lessthan31days).toFixed(2)
                                                var thirty1to45days = parseFloat(data[i].outstanding_data.thirty1to45days).toFixed(2)
                                                var forty6to60days = parseFloat(data[i].outstanding_data.forty6to60days).toFixed(2)
                                                var sixty1to90days = parseFloat(data[i].outstanding_data.sixty1to90days).toFixed(2)
                                                var ninety1to120days = parseFloat(data[i].outstanding_data.ninety1to120days).toFixed(2)
                                                var one21to9999days = parseFloat(data[i].outstanding_data.one21to9999days).toFixed(2)
                                            }

                                            var response = {
                                                dealer_name: dealer_name,
                                                sap_code: sap_code,
                                                as_on_date: date,
                                                net_amount: net_amount,
                                                lessthan31days: lessthan31days,
                                                thirty1to45days: thirty1to45days,
                                                thirty1to45days: thirty1to45days,
                                                forty6to60days: forty6to60days,
                                                sixty1to90days: sixty1to90days,
                                                ninety1to120days: ninety1to120days,
                                                one21to9999days: one21to9999days,
                                            }
                                            OutStandingArray.push(response)
                                        })
                                        OutStanding(OutStandingArray)
                                    }else{
                                        $('.preloader2').css('display','none')
                                    }
                                }
                            })
                        }

                        function OutStanding(data){
                            $("#outstanding_div").dxDataGrid({
                                dataSource: data,
                                //keyExpr: "ID",
                                showBorders: true,
                                paging: {
                                    enabled: false
                                },
                                scrolling: {
                                    mode: "virtual"
                                },
                                rowAlternationEnabled: true,
                                columns: [
                                    {
                                        dataField: "dealer_name",
                                        width: 280,
                                        caption: "Dealer name"
                                    }, {
                                        dataField: "sap_code",
                                        width: 100,
                                        caption: "Sap ID"
                                    }, {
                                        dataField: "as_on_date",
                                        dataType: "date",
                                        format: { skeleton: "d" },
                                        caption: "Date"
                                    }, {
                                        dataField: "net_amount",
                                        caption: "Outstanding"
                                    }, {
                                        dataField: "lessthan31days",
                                        caption: "Less than 31days"
                                    }, {
                                        dataField: "thirty1to45days",
                                        caption: "31 to 45days",
                                    }, {
                                        dataField: "forty6to60days",
                                        caption: "46 to 60days"
                                    },
                                    {
                                        dataField: "sixty1to90days",
                                        caption: "61 to 90days"
                                    },
                                    {
                                        dataField: "ninety1to120days",
                                        caption: "90 to 120days"
                                    },
                                    {
                                        dataField: "one21to9999days",
                                        caption: "More than 121days"
                                    },
                                ],
                                summary: {
                                    totalItems: [
                                    {
                                        column: "net_amount",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "lessthan31days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "thirty1to45days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "forty6to60days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "sixty1to90days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "ninety1to120days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    },
                                    {
                                        column: "one21to9999days",
                                        summaryType: "sum",
                                        displayFormat: "{0}",
                                        showInGroupFooter: true,
                                        customizeText: function (e) {
                                            return  e.value.toFixed(2)
                                        }
                                    }
                                ]
                                }
                            });
                            $('.preloader2').css('display','none')
                        }

                        /**
                         * filter
                         */
                        $('#filter-outstanding').on('click', function(){
                            $('.preloader2').css('display','block')
                            var sap_code = $('.outstanding_dealer_id').val()
                            getOutstandingData(account_id, sap_code)
                            $('#outstanding_div').css('height:180px')
                        })

                        $('#clear-outstanding').on('click', function(){
                            $('.preloader2').css('display','block')
                            $('.outstanding_dealer_id').val(null).trigger('change');
                            var sap_code = ''
                            getOutstandingData(account_id, sap_code)
                        })
                    @endif
                    /**
                     * fetch outstanding data
                     */
                }else{
                    $.ajax({
                        url: '{{route("get-tabs")}}',
                        type: 'GET',
                        headers: {
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                                'Authorization': 'Bearer '+ BEARER
                                },
                        data: {url,account_id,account_type},
                        complete: function(payload, xhr, settings){
                            var newData = payload.responseJSON;
                            if(url == '#documents'){
                                $('.append-class').html(''); //prevent duplicate append
                                var html = '';
                                $.each(newData, function(key,value,e){
                                    var newoption = '';
                                    $.each(value, function(i,value){
                                        var type = value.document_type;
                                        var pdf = '';
                                        var image = '';
                                        if(type == 'png' || type == 'jpeg' || type == 'jpg'){
                                            var image = '<li ><i class="fas fa-image"></i><a href="javascript:void(0)" data-path="'+value.path+'" class="load-doc">'+value.document_name+'</a></li>';
                                        }else{
                                            var pdf = '<li ><i class="fas fa-file-pdf"></i><a href="javascript:void(0)" data-path="'+value.path+'" class="load-doc">'+value.document_name+'</a></li>';
                                        }
                                        newoption += image + pdf
                                    });
                                    var html = '<div class="user-block mt-1 mb-2"><span class="username ml-0"><a href="#">'+key.toUpperCase()+'</a></span></div><div class="col-sm-12"><div class="document_page_list"><ul class="doc-page-list">'+newoption+'</ul></div></div>';
                                    $('.append-class').append(html);
                                });
                            }else if(url == '#location'){
                                map_render(payload);
                            }
                            else if(url == '#activity')
                            {
                                //console.log(newData);
                                var html = '<div class="timeline timeline-inverse">';
                                $.each(newData, function(key,value,e)
                                {
                                    html += '<div class="time-label"><span class="bg-danger">'+key+'</span></div>';
                                    $.each(value, function(i,value){
                                        html+='<div>';
                                        html+='<i class="'+value.icon+' bg-primary"></i>';
                                        html+= '<div class="timeline-item">';
                                        html+= '<span class="time"><i class="far fa-clock"></i> '+value.difference+'</span>';
                                        html+=' <h3 class="timeline-header">'+value.message+'</h3>';
                                        if(value.remarks == '')
                                        {
                                            html+='<div class="timeline-body">'+value.remarks+'</div>';
                                        }
                                        html+= '</div>';
                                        html+='</div>';
                                    });
                                });
                                html += '</div>';
                                $('#activity').html(html);
                            }else if(url == '#followuptab'){
                                $('#table-followup-emp').DataTable().destroy();
                                render_emp_followups();
                            }
                        }
                    });
                }
            });


            /* load image */
            $(document).on('click','.load-doc',function(e){
                e.preventDefault();
                var image = $(this).attr('data-path');
                window.open(image, 'imgWindow');
            });


            /* filter address by type */
            $(document).on('click','.render-filter',function(){
                var address_type = $(this).attr('data-type');
                var url = '#location';
                $.ajax({
                    url: '{{route("get-tabs")}}',
                    type: 'GET',
                    headers: {
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                            'Authorization': 'Bearer '+ BEARER
                            },
                    data: {url,account_id,account_type,address_type},
                    complete: function(payload, xhr, settings){
                        map_render(payload);
                    }
                });
            });


            $(document).on('click','.add-remarks',function(){
            var task_id = $(this).attr('data-id');
            $('#activity_id').val(task_id);
            });


            function followup_function(task_id){
                $.ajax({
                    url: '{{route("fetch-task",0)}}',
                    type: 'GET',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                'Authorization': 'Bearer '+ BEARER
                    },
                    data: {task_id},
                    complete: function(payload, xhr, settings){

                        var newData = payload.responseJSON;
                        //console.log(newData)
                        $("#show_task_remarks").html(newData.task_title);
                        $("#show_task_subject").val(newData.task_description);

                    }
                });

                $("#add-reamrks-form")[0].reset();
                $(".task_dropdown").val([]).trigger("change");
                $("#modal-remarks").modal('show');
            }


            /* map render */
            var map = null;
            var marker = [];
            function map_render(payload){
                var response = payload.responseJSON;
                //console.log(response)
                var LatLong = []
                var addressArr = []
                $.each(response, function(i){
                    var address = response[i][0]
                    var latitude = response[i][1]
                    var longitude = response[i][2]
                    LatLong.push({'latitude': latitude, 'longitude': longitude})
                    //latitudeArr.push(latitude)
                    //longitudeArr.push(longitude)
                    addressArr.push({
                        'latitude' : latitude,
                        'longitude' : longitude,
                        'address' : address
                    })
                })

                var CenterLatLong = averageGeolocation(LatLong)
                var centre = new L.LatLng(CenterLatLong.latitude, CenterLatLong.longitude);
                map = new MapmyIndia.Map('map', {center: centre, zoomControl: true, hybrid: true})
                map.setZoom(7) ///zoom level
                for (var i = 0; i <= addressArr.length - 1; i++) {
                    var postion = new L.LatLng(addressArr[i].latitude, addressArr[i].longitude)
                    marker.push(addMarker(postion, '', addressArr[i].address))
                }

                /* scroll to section */
                $('html, body').animate({
                        scrollTop: $("#map").offset().top
                }, 500);
            }

            function addMarker(position, icon, title, draggable) {
                //var mk = new L.Marker(position, {icon: 'https://www.mapmyindia.com/api/advanced-maps/doc/sample//images/MarkerIcon.png', title: 'hi'});/*marker with a custom icon */
                var mk = new L.Marker(position, {draggable: draggable, title: title});
                mk.bindPopup(title)
                map.addLayer(mk)
                return mk;
            }

            /* to calculate center point by account address lists */
            function averageGeolocation(coords) {
                if (coords.length === 1) {
                    return coords[0];
                }

                let x = 0.0;
                let y = 0.0;
                let z = 0.0;

                for (let coord of coords) {
                    let latitude = coord.latitude * Math.PI / 180;
                    let longitude = coord.longitude * Math.PI / 180;

                    x += Math.cos(latitude) * Math.cos(longitude);
                    y += Math.cos(latitude) * Math.sin(longitude);
                    z += Math.sin(latitude);
                }

                let total = coords.length;

                x = x / total;
                y = y / total;
                z = z / total;

                let centralLongitude = Math.atan2(y, x);
                let centralSquareRoot = Math.sqrt(x * x + y * y);
                let centralLatitude = Math.atan2(z, centralSquareRoot);

                return {
                    latitude: centralLatitude * 180 / Math.PI,
                    longitude: centralLongitude * 180 / Math.PI
                };
            }


            /* tagged account */
            function fetch_data(){
                var dataTable =  $('#mapped-list').DataTable({
                                    processing: true,
                                    serverSide: true,
                                    pageLength: 1,
                                    ajax: {
                                        url: '{{route("get-mapped-accounts")}}',
                                        data: {account_id, account_type}
                                    },
                                    columns: [
                                        { data: 'code'},
                                        { data: 'name'},
                                        { data: 'owner_name'},
                                        { data: 'status'},
                                        { data: 'sap_code'},
                                        { data: 'action'},
                                    ]
                                });
            }


            function account_fetch_data(account_tagged_show){
                var dataTable =  $('#'+account_tagged_show+'-list').DataTable({
                                    processing: true,
                                    serverSide: true,
                                    pageLength: 10,
                                    ajax: {
                                        url: '{{route("get-mapped-accounts")}}',
                                        data: {account_id, account_type,account_tagged_show}
                                    },
                                    columns: [
                                        { data: 'code'},
                                        { data: 'name'},
                                        { data: 'owner_name'},
                                        { data: 'mobile_no'},
                                        { data: 'sap_code'},
                                        { data: 'status'},
                                        { data: 'action'},
                                    ],
                                    "columnDefs": [
                                        {
                                            "targets": [ 4 ],
                                            "visible": (!(account_tagged_show == 'mason_tagged_show' || account_tagged_show == 'pcontactor_tagged_show' || account_tagged_show == 'engineer_tagged_show' )) ? true :false
                                        },
                                        {
                                            "targets": [ 2 ],
                                            "visible": (!(account_tagged_show == 'mason_tagged_show' || account_tagged_show == 'pcontactor_tagged_show' || account_tagged_show == 'engineer_tagged_show' )) ? true :false
                                        },
                                        {
                                            "targets": [ 6 ],
                                            "searchable": false,
                                            'orderable': false,
                                        }
                                    ]
                                });
            }


            $('.tab-class-account').on('click',function(){
                $('#mapped-list').DataTable().destroy();
                fetch_data();
            })


            $('.tab-class-account-tagged').on('click',function(){
                var datatable_id = $(this).attr('id');
                // //alert('hello');
                $('#'+datatable_id+'-list').DataTable().destroy();
                account_fetch_data(datatable_id);
                //
                // fetch_data();
            })


            /* get block list */
            $(document).on('change','.operation_district',function(event){
                var district_id = '';
                $('.operation_district').each(function() {
                    // this should loop through all the selected elements
                    district_id = $(this).val();
                });
                var state_id =  $("#operation_state").val();
                var type = 3;
                $.ajax({
                    url: '{{route("get-block-by-district")}}',
                    type: 'GET',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Authorization': 'Bearer '+ BEARER
                        },
                    data: {state_id,district_id,type},
                    complete: function(payload){
                        var newData = payload.responseJSON;
                        var content = '<option></option>';
                        $.each(newData.data, function(i, item) {
                            content =content+' <optgroup label="'+i+'">';
                            $.each(item, function(j, value) {
                                content =content+ '<option value="'+value.block_id+'">'+value.block_name+'</option>';
                            });
                            content = content +'</optgroup>';
                        });
                        $("#operation_block_id").html(content);
                        $("#operation_block_id").val([]).trigger('change');
                    }
                });
            });


            $(document).on('change','#operation_area_type',function(event){
                //var block_id = []
                //$('#operation_block_id').each(function() {
                   block_id = $('#operation_block_id').val()
                //})

                console.log(block_id)
                var block_type =  $(this).val()
                $.ajax({
                    url: '{{route("areaByBlockType")}}',
                    type: 'GET',
                    headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                //'Authorization': 'Bearer '+ BEARER
                        },
                    type: 'POST',
                    url: '{{route("get-account-callactivity-list")}}',
                    data: {account_id,account_type}
                },
                columns: [
                    { "render": function(data, type, full, meta) {
                                return i++;
                            }
                        },
                    { data: 'stage_of_construction'},
                    { data: 'stage_of_work'},
                    { data: 'requirement_val'},
                    { data: 'plot_area'},
                    { data: 'sentiment'},
                    { data: 'sentiment_action'},
                    { data: 'ihb_remarks'},
                    { data: 'name'},
                    {   data: 'call_time',
                            type: 'num',
                            render: {
                                _: 'display',
                                sort: 'timestamp'
                            }
                        },
                    { data: 'call_duration'}
                ]
            });
    }

    function ihb_tagged_details_fn(listshow)
    {
        var ACCOUNT_TYPE = 48;
        var ownerID = account_id;
        var dataTable =  $('#ihbaccount-ihb_project_tagged').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            //'Authorization': 'Bearer '+ BEARER
                    },
                url: '{{route("get-ihb-project-list")}}',
                data: {ACCOUNT_TYPE,ownerID}
            },
            columns: [
                { data: 'ihb_code'},
                { data: 'project_name'},
                { data: 'mobile_no'},
                { data: 'state_name',name:'state_id'},
                { data: 'district_name',name:'district_id'},
                { data: 'status',name:'status'},
                { data: 'project_type',name:'project_type'},
                { data: 'created_user',name:'referred_by'},
                { data: 'responsible_name',name:'user_id'},
                 { data: 'action',name:'ihb_code'}
            ]
        });
    }
    /* tab render */
    $('.tab-class').on('click',function(){
        var url = $(this).attr('href');

        /* an loader */
        if(url == '#sales_details' || url == '#primary-item-show')
        {
            $('#dispatch_item_show-list').DataTable().destroy();
            primary_sales_info();
        }
        else if(url == '#secondary_item_show')
        {
            $('#secondary_item_show-list').DataTable().destroy();
            secondary_sales_info();
        }
        else if(url == '#ihb_call_details')
        {
            $('#ihb_call_details-list').DataTable().destroy();
            ihb_call_details_fn();
        }
        else if(url == '#ihb_project_tagged')
        {
            $('#ihbaccount-ihb_project_tagged').DataTable().destroy();
            ihb_tagged_details_fn('ihb_project_tagged');
        }
        else
        {
            $.ajax({
                url: '{{route("get-tabs")}}',
                type: 'GET',
                headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                        'Authorization': 'Bearer '+ BEARER
                        },
                    data: {block_id,block_type},
                    complete: function(payload){
                        var newData = payload.responseJSON
                        $('#operation_area').find('option').not(':first').remove()
                        content = ''
                        $.each(newData.data, function(i) {
                            content = content + '<option value="'+newData.data[i].area_id+'">'+newData.data[i].area_name+'</option>';
                        });
                        $("#operation_area").html(content);
                        $("#operation_area").val([]).trigger('change');
                    }
                });
            });

            address_shopwing('S',0);
            $('#add_more_button').on('click',function(){
                var total_address     = $("#total_address").val();
                var new_address_count = parseInt(total_address)+1;
                $("#total_address").val(new_address_count);
                address_shopwing('A',new_address_count);
            });

            function address_shopwing(fun_type,position){
                $.ajax({
                        url: '{{route("account-address-show")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    'Authorization': 'Bearer '+ BEARER
                        },
                        data: {account_id, account_type,fun_type,position},
                        success: function(data){

                        },
                        complete: function(payload, xhr, settings){
                            if(fun_type == 'S'){
                                $("#address_content_show").html(payload.responseText);
                            }else{
                                $("#address_content_show").append(payload.responseText);
                            }
                        }
                    });
            }

            $('#rescheduled_date').val(TODAY)
            /* enable datepicker */
            $('#rescheduled').on('change',function(){
                if($(this).prop("checked") == true){
                    $('#rescheduled_date').removeAttr('disabled')
                    $('#rescheduled_date').val(TODAY)
                }else{
                    $('#rescheduled_date').prop('disabled', true)
                }
            })

            /** prevents from entering non digit keys */

            //custom function for sap search
            var defaultMatcher = $.fn.select2.defaults.defaults.matcher
            function matchCustom(params, data){
                if($.trim(params.term) === '') {
                    return data
                }

                if (typeof data.text === 'undefined'){
                    return null;
                }

                if($.isNumeric(params.term)){
                    if(data.id.indexOf(params.term) > -1){
                        var modifiedData = $.extend({}, data, true);
                        modifiedData.text += ''
                        return modifiedData
                    }
                }else{
                    return defaultMatcher(params, data)
                }

                return null
            }

            $(".dealers_list").select2({
                theme: 'bootstrap4',
                matcher: matchCustom,
                searchInputPlaceholder: 'Search by dealer name or sap id'
            })

            //set placeholder in dealer list
            $('.dealers_list').one('select2:open', function(e) {
                $('input.select2-search__field').prop('placeholder', 'Search by dealer name or sap id')
            })


            //temp fix
            $('#business_type').on('change', function(){
                var business_type = $(this).val()
                console.log(business_type)
                if(business_type ==1 || business_type == 3 || business_type == 2){
                    $('#partner-set').css('display','block')
                    $('#individual-set').css('display','none')
                }else{
                    $('#partner-set').css('display','none')
                    $('#individual-set').css('display','block')
                }
            })
        </script>
