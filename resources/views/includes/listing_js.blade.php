<script type="text/javascript">
 /* change assigned task status */
 const swalButtons = Swal.mixin({
                                    customClass: {
                                    confirmButton: 'btn btn-success swal_btn',
                                    cancelButton:  'btn btn-danger swal_btn'
                                    },
                                    buttonsStyling: false
    })
    /***** change the status  *******/
    $(document).on('click','.status_change_model',function(){
       var account_id = $(this).attr('data-id');
        swalButtons.fire({
            title: 'Change Account Status',
            input: 'select',
            inputOptions: {
                6: 'Suspended',
                7: 'In-active',
                9: 'Compliance',
                10: 'Legal',
                11: 'Close'
            },
            inputPlaceholder: 'Select a status',
            showCancelButton: true,
            inputValidator: (value) => {
                return new Promise((resolve) => {
                if (value != '') {
                    $.ajax({
                        url: '',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    'Authorization': 'Bearer '+ BEARER
                                    },
                        data: {ACCOUNT_TYPE,account_id,value},
                        success: function (data) {
                            if(data.status == 'success'){
                                Swal.fire(
                                        'Activated!',
                                        'Successfully change account status',
                                        'success'
                                    )
                            }
                            else
                            {
                                Swal.fire(
                                        'de-Activated!',
                                        'Already change this account status',
                                        'error'
                                    )
                            }
                        }
                    });
                } else {
                    resolve('You need to select status')
                }
                })
            }
        });



    });

    $(document).on('change','.state_dropdown', function(){
        let state_id = $(this).val()
        state_id = [state_id]
        getDistricts(state_id)
    })

    function getDistricts(state_id){
        $('.preloader2').css('display','block')
            var type = 0
            $.ajax({
            url: "{{route('area.getDistrictByState')}}",
            type: 'GET',
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),  'Authorization': 'Bearer '+ BEARER},
            data: {state_id, type},
            complete: function(payload){
                var data = payload.responseJSON.data
                $('.district_dropdown').find('option').not(':first').remove()
                let html = ''
                $.each(data, function(i){
                    html += `<option value="${data[i][0].district_id}">${data[i][0].district_name}</option>`
                })
                $('.district_dropdown').append(html)
            }
        })
        $('.preloader2').css('display','none')
    }
</script>
