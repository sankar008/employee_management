<!-- Main Footer -->
<footer class="main-footer">
    <strong>Copyright &copy; {{ date('Y') }} <a
            href="{{ config('app.url') }}">{{ config('app.company_name') }}</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
        <b>Version</b> {{ config('app.app_version') }}
    </div>
</footer>
<!-- scripts -->
<script src="{{ asset('assets/plugins/jquery/jquery.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<script src="{{ asset('assets/plugins/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
<script src="{{ asset('assets/dist/js/adminlte.js') }}"></script>
<script src="{{ asset('assets/dist/js/demo.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('assets/plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('assets/plugins/chart.js/Chart.min.js') }}"></script>
<!-- PAGE SCRIPTS -->
<script src="{{ asset('assets/plugins/select2/js/select2.full.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/plugins/toastr/toastr.min.js') }}"></script>
<script src="{{ asset('assets/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-validation/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-validation/additional-methods.min.js') }}"></script>
<script src="{{ asset('assets/plugins/fancybox/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jszip/jszip.js') }}"></script>
<script src="{{ asset('assets/plugins/dx/js/dx.all.js') }}"></script>
<script src="{{ asset('assets/plugins/dx/js/dx.aspnet.data.js') }}"></script>
@if (in_array(Route::currentRouteName(), [
        'account.distributors.list',
        'dealer-list',
        'sub-dealer-list',
        'account.engineers.list',
        'account.masons.list',
        'account.pettycontractors.list',
        'web.order.ho.approval',
        'web.order.sap_checker',
        'web.order.summary',
        'account.fabricators.list',
        'activity-report',
        'engineer.points.summary.report',
        'account.conversion.index',
        'account.dashboard',
        'employee.account.tag.report',
        'ihb.govt.dept.list',
        'golok.payment.logs',
    ]))
    <script src="{{ asset('assets/plugins/dx/js/polyfill.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dx/js/exceljs.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/dx/js/FileSaver.min.js') }}"></script>
@endif
