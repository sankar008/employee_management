


<!-- pofile data modal -->
<div class="modal fade show" id="calling-ihb-project-modal" aria-modal="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">IHB Project Calling Information <span class="timercount mr-2" id="timer-count"></span></h4>
                <button type="button" class="class_cancel_button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="ihb-call-body">
                
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
        <!-- /.modal-dialog -->
</div>
    <!-- pofile data modal ends-->

<script type="text/javascript">
    
    const url_page_link = '{{ Request::url() }}';
    $(document).on('click','.call_action_icon',function()
    {
        var project_id = $(this).attr('data-id');
        var type = 'calling-body';
        $.ajax({
            url: '{{route("ajax-get-project-info")}}',
            type: 'POST',
            headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Authorization': 'Bearer '+ BEARER
            },
            data: {project_id,type},
            success: function(data){
                console.log(data);
                $("#calling-ihb-project-modal").show();
                $("#ihb-call-body").html(data);
                $("#ihb_project_id").val(project_id);
                $("#page_link").val(url_page_link);
            }
        });
    });
    $('#calling-ihb-project-modal').on('hidden.bs.modal', function () 
    {
        $("#calling-lead-form").validate().resetForm();
        $('#calling-lead-form input').removeClass('error');
        $('#calling-lead-form select').removeClass('error');
        $('#calling-lead-form input').trigger("reset");
        $('#calling-lead-form input').css('pointer-events','all');
    });
    $(document).on('click','.class_cancel_button',function()
    {
        var call_duration_time    = $("#call_duration_time").val();
        if(call_duration_time == '')
        {
            $("#calling-ihb-project-modal").hide();
        }
        else
        {
            var ihb_project_id      = $("#ihb_project_id").val();
            var call_response_id    = $("#call_response_id").val();
            var call_activity_id    = $("#call_activity_id").val();
            $.ajax({
                        url: '{{route("track-ihb-call-cancel")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ BEARER
                                },
                        data: {ihb_project_id,call_activity_id,call_duration_time,call_response_id},
                        success: function (data) {
                            $("#calling-ihb-project-modal").hide();
                        }
                });
        }
    });
    $(document).on('change','#sentiment_type',function(){
        var sentiment_type_id = $(this).val();
        $.ajax({
            url: '{{route("get-sentiment-action-list")}}',
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ BEARER
                },
            data: {sentiment_type_id},
            complete: function(payload){
                var newData = payload.responseJSON;
                console.log(newData);
                var content = '<option></option>';
                $.each(newData, function(i, item) {
                    content =content+ '<option value="'+item.sentiment_id+'">'+item.sentiment+'</option>';
                });
                $("#action_type").html(content);
                $("#action_type").val([]).trigger('change');
            }
        });  
    });
    $(document).on('change','#stage_construction_id',function(){
        var stage_construction_id = $(this).val();
        $.ajax({
            url: '{{route("get-stage-cons-list")}}',
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ BEARER
                },
            data: {stage_construction_id},
            complete: function(payload){
                var newData = payload.responseJSON;
                console.log(newData);
                var content = '<option></option>';
                $.each(newData, function(i, item) {
                    content =content+ '<option value="'+item.stage_construction_id+'">'+item.stage_name+'</option>';
                });
                $("#stage_work_id").html(content);
                $("#stage_work_id").val([]).trigger('change');
            }
        });  
    });
      
    
    // $(document).on('click','.nav-link',function(){
    //     var href = $(this).attr('href');
    //     if(href == '#call-activity')
    //     {
    //         var call_lead_id = $("#call_lead_id").val();
            
    //         $("#call-activity-list").dataTable().fnDestroy()
    //         var dataTable =  $('#call-activity-list').DataTable({
    //                 processing: true,
    //                 serverSide: true,
    //                 autoWidth:true,
    //                 language: {
    //                     sProcessing: "<div id='tableloader'><div id='inner_processing'><img src='{{asset('assets/images/loader4.gif')}}'></div></div>"
    //                 },
    //                 drawCallback: function() {
    //                     tooltip()
    //                 }, 
    //                 ajax:{
    //                     "url" : '{{route("fetch-callactivity-list")}}',
    //                     headers: {
    //                         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
    //                         'Authorization': 'Bearer '+ BEARER
    //                     },
    //                     "type": "POST",
    //                     "data" : {call_lead_id}
    //                 },
    //                 columns: [
    //                             { data: 'sl_no'},
    //                             { data: 'call_respone_id'},
    //                             { data: 'call_remarks',
    //                                 render: function ( data, type, row ) {
    //                                     return  data;
    //                                     }
    //                             },
    //                             { data: 'duration_time'},
    //                             { data: 'start_time'},
    //                             { data: 'call_by'},
    //                         ]
    //         });
    //     }
    // });
    


    $(document).on('click','#start-ihb-call',function()
    {
        var ihb_project_id = $("#ihb_project_id").val();
        var page_link = $("#page_link").val();
        var btn = $(this);
         if(ihb_project_id != ''){
          swalButtons.fire({
                            title: 'Are you sure?',
                            text: "You want to Call this IHB Project?",
                            icon: 'warning',
                            showCancelButton: true,
                            confirmButtonText: 'Confirm',
                            cancelButtonText: 'Cancel',
                            reverseButtons: true
            }).then((result) => {
                if(result.value){
                  $.ajax({
                        url: '{{route("click-to-ihb-call")}}',
                        type: 'POST',
                        headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                                    'Authorization': 'Bearer '+ BEARER
                                  },
                        data: {ihb_project_id,page_link},
                        success: function (data) {
                            if(data.status == 'success'){
                                $(".timercount").show();
                                $("#call_info_update").attr('disabled',false);
                                $("#call_activity_id").val(data.call_insert_id);
                                $("#call_response_id").val(data.call_id); 
                                $("#calling_activites_id").val(data.call_activities_id); 
                                mins = 0
                                secs = 0;
                                btn.attr("disabled", true);
                                interval = setInterval(function() {
                                if (mins >= 0 && secs >= 0) {
                                    if(secs >= 0 && secs <= 59)
                                    {
                                        secs++;
                                    }
                                    else
                                    {
                                        secs = 0;
                                    mins++
                                    }
                                }
                                $("#timer-count").html(pad(mins, 2) + ":" + pad(secs, 2));
                                $("#call_duration_time").val(pad(mins, 2) + ":" + pad(secs, 2));
                                }, 1000);
                                
                            }
                        }
                  });
                }
            });
        }
    });
    function pad(num, size) {
        var s = num + "";
        while (s.length < size) s = "0" + s;
        return s;
    }

    

   
</script>