<!-- remarks modal -->
    <div class="modal fade show" id="modal-remarks" aria-modal="true" style="padding-right: 17px">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add remarks</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body pt-1">
                  <form action="javascript:void(0)" id ="add-reamrks-form">
                      <div class="row">
                        <div class="card-body col-md-4 card-body2 pt-0">
                          <div class="col-md-12 pl-0">
                                <label for="exampleInputTime">Task Subject</label>
                                <input type="text" class="form-control" id="show_task_subject" name="show_task_subject" readonly>
                            </div>
                          <label for="exampleInputTime">Is rescheduled? <span class="required_star">*</span></label>
                          <div class="form-group inner-addon right-addon pt-2">
                            <div class="icheck-primary d-inline">
                              <input class="mclass2 cclass38 cclass" type="checkbox" name="rescheduled" id="rescheduled" value="1">
                              <label for="rescheduled">Yes</label>
                            </div>
                          </div>
                          <label for="exampleInputTime">Rescheduled date? <span class="required_star">*</span></label>
                          <input type="text" class="datepicker selfdate form-control" id="rescheduled_date" name="rescheduled_date" disabled>

                          <div class="form-group inner-addon right-addon pt-2" style="display:none;" id="account_convert_div">
                            <div class="icheck-primary d-inline">
                              <input class="mclass2 cclass38 cclass" type="checkbox" name="account_confirm" id="account_confirm" value="1">
                              <label for="account_confirm">Convert to Account</label>
                            </div>
                          </div>
                        </div>
                        <div class="card-body col-md-8 card-body2 pl-1 pt-0">
                          <div class="col-md-12">
                            <label for="exampleInputTime">Task Remarks </label>
                            <textarea class="form-control followup_remark" id="show_task_remarks" name="show_task_remarks" placeholder="Type remark" cols="5" rows="2" style="width:100%" readonly></textarea>
                          </div>
                          <div class="col-md-12">
                            <label for="exampleInputTime">Remarks <span class="required_star">*</span></label>
                            <textarea class="form-control followup_remark" id="remarkT" name="remarkT" placeholder="Type remark" cols="5" rows="2" style="width:100%"></textarea>
                          </div>
                          <div class="col-md-12">
                            <label for="exampleInputTime">Followup type <span class="required_star">*</span></label>
                            <div class="d-flex justify-content-between align-items-center">
                              <select class="form-control select2bs4 task_dropdown followupchange" data-placeholder="Select followup type" id="fType" name="fType">
                                    <option></option>
                                    @foreach($get_task_type_list as $tasktypelist)
                                        <option value="{{$tasktypelist->event_id}}" ptaskname="{{$tasktypelist->event_name}}">{{$tasktypelist->event_name}}</option>
                                    @endforeach
                              </select>
                              <input type="submit" class="btn btn-primary add-reamrks-btn ml-3" data-type="other" value="Add remark">
                            </div>
                          </div>
                        </div>
                      </div>
                  </form>
            </div>
          </div>
        </div>
    </div>

    <!-- self followup -->
    <div class="modal fade show" id="modal-self" aria-modal="true" style="padding-right: 17px;">
        <div class="modal-dialog modal-lg modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title" id="tasktitle">Add Self Task</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body pt-1">
            <form action="javascript:void(0)" method="post" id="add-task-lead-form">
                    <div class="row">
                        <div class="card-body col-md-6 card-body2">
                            <div class="form-group inner-addon right-addon" id="resp_emp_show">
                                    <label for="exampleInputEmail1">Responsible List<span class="required_star">*</span></label>
                                    <select class="form-control select2bs4 task_dropdown" data-placeholder="Select Responsible employee" id="resp_emp_id" name="resp_emp_id">
                                    <option></option>
                                        @foreach($all_field_emp as $emplist)
                                            <option value="{{$emplist->get_user_details->id}}">{{$emplist->get_user_details->name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="form-group inner-addon right-addon">
                                    <label for="exampleInputEmail1">Task Priority <span class="required_star">*</span></label>
                                    <select class="form-control select2bs4 task_dropdown" data-placeholder="Select task priority list" id="task_priority_id" name="task_priority_id">
                                    <option></option>
                                        @foreach($get_task_priority_list as $priporitylist)
                                            <option value="{{$priporitylist->priority_id}}">{{$priporitylist->priority_name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="row custom_radio_class"><label for="task_priority_id" class="error" style="display:none;"></label></div>
                            </div>
                            <div class="form-group inner-addon right-addon">
                                    <label for="exampleInputEmail1">Type task <span class="required_star">*</span></label>
                                    <select class="form-control select2bs4 task_dropdown" data-placeholder="Select task type list" id="task_type_id" name="task_type_id">
                                    <option></option>
                                        @foreach($get_task_type_list as $tasktypelist)
                                            <option value="{{$tasktypelist->event_id}}" ptaskname="{{$tasktypelist->event_name}}">{{$tasktypelist->event_name}}</option>
                                        @endforeach
                                    </select>
                                    <div class="row custom_radio_class"><label for="task_type_id" class="error" style="display:none;"></label></div>
                            </div>
                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1"> Task Date <span class="required_star">*</span></label>
                                <input type="text" class="datepicker form-control task_date" id="task_date" name="task_date">
                            </div>
                        </div>
                        <div class="card-body col-md-6 card-body2">
                            <div class="form-group inner-addon right-addon">
                                    <label for="exampleInputEmail1">Task Purpose</label>
                                    <select class="form-control select2bs4 task_dropdown" data-placeholder="Select task purpose list" id="task_purpose_id" name="task_purpose_id">
                                    <option></option>
                                        @foreach($get_task_purpose_list as $purpose_list)
                                            <option value="{{$purpose_list->purpose_id}}">{{$purpose_list->purpose_name}}</option>
                                        @endforeach
                                    </select>
                            </div>
                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1"> Title <span class="required_star">*</span></label>
                                <input type="text" class="form-control" placeholder="Subject" id="task_title" name="task_title"><i class="fas fa-info-circle" style="top:25px;" data-toggle="tooltip" data-placement="top" title="Task Subject can't contain number, special charecter or empty space at the end"></i>
                            </div>
                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1"> Description <span class="required_star">*</span></label>
                                <textarea type="text" class="form-control" placeholder="Task description" id="task_description" name="task_description"></textarea>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" value="self" id="add-followup-type">
                    <input type="hidden" value="{{ $details->lead_id }}" id="lead_id" name="lead_id">
                    <input type="hidden" value="" id="selftask_assign" name="selftask_assign">
                    <div class="row" style="float:right;">
                        <div class="col-4" style="flex:none;max-width:100%">
                        <input type="submit" class="btn btn-primary" id="11" value="Save" data-pid="1">
                            <input type="submit" class="btn btn-success" id="22" value="Save & Add New" data-pid="2">
                            <input type="button" class="btn btn-info data-form-reset" value="Reset">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
               
            </div>
          </div>
        </div>
    </div>

    <!-- comment modal -->
    <div class="modal fade show" id="modal-remarks-render" aria-modal="true" style="padding-right: 17px">
      <div class="modal-dialog modal-lg modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Remarks</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body pt-1">
                <div id="remarks-render"></div>
            </div>
        </div>
      </div>
    </div>

    <input type="hidden" id="activity_id" value="0">