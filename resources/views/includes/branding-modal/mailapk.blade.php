<!-- import modal -->
<div class="modal fade" id="brand-apk-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="overlay d-flex justify-content-center align-items-center loader">
                <img src="{{ asset('assets/images/spinner.gif') }}" alt="">
            </div>
            <div class="modal-header">
                <h4 class="modal-title">Branding APK</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" method="post" id="branding_apk_form">
                    <div class="row">
                        <div class="col-lg-12 form-group inner-addon right-addon">
                            <label class="d-block text-left" for="BrandingAPK">Branding Link <span class="required_star">*</span></label>
                            <div class="input-group">
                                    <input type="text" class="form-control branding-apk-link" name="branding_apk_link">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="float:right;">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <button type="submit" class="btn btn-primary brandmailapk" >Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
