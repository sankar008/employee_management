        <div class="container-fluid">
            <div class="row mb-2">
            <div class="col-sm-12 text-center">
                <h1 class="m-0 text-dark pt-1" style="font-size:24px"><strong id="heading_title">{{strtoupper($page_title??'')}}</strong></h1>
            </div><!-- /.col -->
            <div class="col-sm-12 float-right d-none">
                <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('document.dashboard') }}">Dashboard</a></li>
                @if(Route::current()->getName() == 'get-lead-information')
                 <li class="breadcrumb-item"><a href="{{ route('get.Leadist') }}">Manage Lead</a></li>
                 @endif
                @if(Route::current()->getName() == 'get-user-profile2' && Auth::user()->id == 1)
                    <li class="breadcrumb-item"><a href="{{ route('user-list') }}">Users</a></li>
                @endif
                <li class="breadcrumb-item active">{{$page_title}}</li>
                </ol>
            </div>
            </div><!-- /.row -->
        </div>
