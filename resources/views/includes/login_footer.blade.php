    <!-- Bootstrap 4 -->
    <script src="{{asset('assets/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <!-- AdminLTE App -->
    <script src="{{asset('assets/dist/js/adminlte.min.js')}}"></script>
    <script src="{{asset('assets/plugins/toastr/toastr.min.js')}}"></script>
    <script src="{{asset('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script>

        $('#login_form').validate({
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                }
            },
            messages: {
                username: {
                    required: 'Please enter your username'
                },
                password: {
                    required: 'Please enter your password'
                }
            },
            submitHandler: function(e) {
                var email  = $('#email').val();
                var password  = $('#password').val();
                var remember  = 0;
                if($("#remember").prop("checked") == true){
                    remember = 1;
                }
                let prevurl = '';

                $.ajax({
                    url: '{{route("login")}}',
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {email, password, remember},
                    complete:function(payload){
                        console.log(payload);
                        return false;
                        var response = payload.responseJSON;

                        if(payload.status == 200){
                            if (response.is_true_user === true) {
                                $('#otp_email_div').removeClass('d-none');
                                $('#is_true_user').val('true');
                            } else {
                                window.location.href = response.prevURL;
                            }
                        }else if(payload.status == 404){
                            toastr.error("Sorry we didn\'t find any user with these credentials")
                        }else if(payload.status == 403){
                            toastr.error(response.message);
                        }else if(payload.status == 429){
                            var responseJSON = payload.responseJSON
                            toastr.error('Too many failed attemps. Please try again after ' + responseJSON.minutes + ' minutes')
                        }else if(payload.status == 201){
                            toastr.success('OTP sent successfully to your preferred communication method')
                            setTimeout(() => {
                                window.location.href = "#";
                            }, 2000);
                        }else{
                            toastr.error('Something went wrong please try again later')
                        }
                    }
                });
            }
        });

    </script>
