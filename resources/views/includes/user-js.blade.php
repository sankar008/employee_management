<script>

$(document).on('click','.account-list',function(){
    var url = $(this).attr('href');
    var ACCOUNT_TYPE = 18;
    var pageUrl = 'userdetails';
    var tableid = '';
    if(url == '#distributorslist' || url == '#tagged_account'){
        tableid = '#distributorslist';
        ACCOUNT_TYPE = 18;
        $('#distributorslist-list').DataTable().destroy();

    }else if(url == '#dealerslist'){
        tableid = '#dealerslist';
        ACCOUNT_TYPE = 39;
        $('#dealerslist-list').DataTable().destroy();

    }else if(url == '#subdealerslist'){
        tableid = '#subdealerslist';
        ACCOUNT_TYPE = 40;
        $('#subdealerslist-list').DataTable().destroy();

    }else if(url == '#masonlist' || url == '#influnceraccount'){
        tableid = '#masonlist';
        ACCOUNT_TYPE = 45;
        $('#masonlist-list').DataTable().destroy();
    }else if(url == '#engineerlist'){
        tableid = '#engineerlist';
        ACCOUNT_TYPE = 44;
        $('#engineerlist-list').DataTable().destroy();

    }else if(url == '#pclist'){
        tableid = '#pclist';
        ACCOUNT_TYPE = 46;
        $('#pclist-list').DataTable().destroy();

    }else if(url == '#projectlist'){
        tableid = '#projectlist';
        ACCOUNT_TYPE = 48;
        $('#projectlist-list').DataTable().destroy();

    }else if(url == '#consumerlist'){
        tableid = '#consumerlist';
        ACCOUNT_TYPE = 47;
        $('#consumerlist-list').DataTable().destroy();
    }

    fetch_account_data(ACCOUNT_TYPE,pageUrl,profile_user_id,profile_mob_id,profile_id,tableid);
});



function fetch_account_data(ACCOUNT_TYPE,pageUrl,profile_user_id,profile_mob_id,profile_id,tableid){
    var routeUrl = '';
    var columndef = [];
    var column = [];
    if(tableid == '#distributorslist'){
        column = [
                    { data: 'distributor_code'},
                    { data: 'state'},
                    { data: 'distributorship_name'},
                    { data: 'owner_name'},
                    { data: 'sap_code'},
                    { data: 'status'},
                ];
        columndef =  [ 
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": false
                    }
            ];
        routeUrl = '{{route("get-accounts")}}';
    }
    else if(tableid == '#dealerslist')
    {
        column = [
                { data: 'dealer_code'},
                { data: 'state'},
                { data: 'dealership_name'},
                { data: 'owner_name'},
                { data: 'sap_code'},
                { data: 'status'}
            ];
        columndef =  [ 
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": false
                    },
            ];
        routeUrl = '{{route("get-accounts")}}';
    }
    else if(tableid == '#subdealerslist')
    {
        column = [
                { data: 'subdealer_code'},
                { data: 'state'},
                { data: 'subdealership_name'},
                { data: 'owner_name'},
                { data: 'status'}
            ];
        columndef =  [ 
                    {
                        "targets": [ 1 ],
                        "visible": false,
                        "searchable": false
                    },
            ];
        routeUrl = '{{route("get-accounts")}}';
    }else if(tableid == '#masonlist'){
        column = [
                { data: 'mason_code'},
                { data: 'mason_name'},
                { data: 'mobile_no'},
                { data: 'state'},
                { data: 'dealer_name'},
                { data: 'status'}
            ];
        routeUrl = '{{route("get-influncer-accounts")}}';
        columndef =  [ 
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": false
                    }
            ];
    }else if(tableid == '#engineerlist'){
        column = [
                { data: 'engineer_code'},
                { data: 'engineer_name'},
                { data: 'mobile_no'},
                { data: 'state'},
                { data: 'dealer_name'},
                { data: 'status'},
                // { data: 'location'}
            ];
        routeUrl = '{{route("get-influncer-accounts")}}';
        columndef =  [ 
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": false
                    },
            ];
    }else if(tableid == '#pclist'){
        column = [
                { data: 'pc_code'},
                { data: 'pc_name'},
                { data: 'mobile_no'},
                { data: 'state'},
                { data: 'dealer_name'},
                { data: 'status'}
            ];
        columndef =  [ 
                    {
                        "targets": [ 3 ],
                        "visible": false,
                        "searchable": false
                    }
            ];
        routeUrl = '{{route("get-influncer-accounts")}}';
    }else if(tableid == '#projectlist'){
        column = [
                { data: 'ihb_code'},
                { data: 'project_name'},
                { data: 'mobile_no'},
                { data: 'state_id',name:'state_id'},
                { data: 'district_id',name:'district_id'},
                { data: 'status',name:'status'},
                { data: 'project_type',name:'project_type'},
                { data: 'referred_by',name:'referred_by'},
                { data: 'user_id',name:'user_id'},
                { data: 'action',name:'ihb_code'}
            ];
        routeUrl = '{{route("get-ihb-project-list")}}';
    }else if(tableid == '#consumerlist'){
        column = [
                { data: 'ihb_code'},
                { data: 'project_name'},
                { data: 'mobile_no'},
                { data: 'state_id',name:'state_id'},
                { data: 'district_id',name:'district_id'},
                { data: 'status',name:'status'},
                { data: 'project_type',name:'project_type'},
                { data: 'referred_by',name:'referred_by'},
                { data: 'user_id',name:'user_id'},
                { data: 'action',name:'ihb_code'}
            ];
        routeUrl = '{{route("get-ihb-project-list")}}';
    }
    
    var dataTable =  $(tableid+'-list').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            //'Authorization': 'Bearer '+ BEARER
                    },
                url: routeUrl,
                data: {ACCOUNT_TYPE,pageUrl,profile_user_id,profile_mob_id,profile_id}
            },
            columns:  column,
            "columnDefs": columndef
            
    });
}


$(document).on('click','.account-task',function(){
    var url = $(this).attr('href');
    var pageUrl = 'userdetails';
    var tableid = '';
    var tab = 'daily';
    if(url == '#accounttask' || url == '#daily_accounttask')
    {
        tab = 'daily';
        tableid = '#daily_accounttask';
        $('#daily_accounttask-list').DataTable().destroy();
    }
    else if(url == '#weekly_accounttask')
    {
        tab = 'weekly';
        tableid = '#weekly_accounttask';
        $('#weekly_accounttask-list').DataTable().destroy();
    }
    else if(url == '#monthly_accounttask')
    {
        tab = 'monthly';
        tableid = '#monthly_accounttask';
        $('#monthly_accounttask-list').DataTable().destroy();
    }
    else if(url == '#allpast_accounttask')
    {
        tab = 'past';
        tableid = '#allpast_accounttask';
        $('#allpast_accounttask-list').DataTable().destroy();
    }
    fetch_account_task_data(pageUrl,tableid,profile_user_id,tab);
});



function fetch_account_task_data(pageUrl,tableid,profile_user_id,tab){
    var url = '';
    var dataTable =  $(tableid+'-list').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                "url" : '{{route("fetch-tasklist")}}',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                    'Authorization': 'Bearer '+ BEARER
                },
                "type": "POST",
                "data" : { 
                    pageUrl,profile_user_id,tab
                }
            },
            columns: [
                        { data: 'hide_task_date'},
                        { data: 'priority',
                            'render': function (id) 
                            {
                                return id == 2 ? '<small class="Badge Badge-warning p-1">High</small>': id == 3 ? '<small class="Badge Badge-primary p-1">Medium</small>' : id == 1 ? '<small class="Badge Badge-danger p-1">Very High</small>' : id == 4 ? '<small class="Badge Badge-success p-1">LOW</small>' : ''
                            }
                        },
                        { data: 'task_date','render':function(id)
                            {
                                var dateinfo = id.split('@');
                                return ((dateinfo[0] >= 0 && dateinfo[0] < 2) ? '<span class="urgent"></span>' : (dateinfo[0] >= 2 && dateinfo[0] < 3) ? '<span class="semiurgent"></span>' :(dateinfo[0] >= 3 && dateinfo[0] < 5)? '<span class="semilow"></span>' : (dateinfo[0] >= 5) ? '<span class="verylow"></span>' :'<span class="missed"></span>' )+dateinfo[1];
                            }
                        },
                        { data: 'account_type'},
                        { data: 'account_name'},
                        { data: 'task_type'},
                        { data: 'status',
                            'render': function (id) 
                            {
                                return id == 2 ? '<span class="rescheduled_task">Rescheduled</span>': id == 0 ? '<span class="new_task">New task</span>' : id == 1 ? '<span class="completed_task">Completed</span>' : id == 4 ? '<span class="deleted_task">Deleted</span>' : id == 3 ? '<span class="missed_task">Missed</span>' : ''
                            }
                        },
                        { data: 'task_title'},
                        { data: 'task_description',
                            render: function ( data, type, row ) {
                                return data.length > 15 ?
                                    data.substr( 0, 15 ) +' <span class="dot-right"> <i class="fas fa-ellipsis-h"></i></span> <div class="full_description" style="display:none">'+data+'</div>' :
                                    data;
                            }
                        },
                        { data: 'assign_to'},
                        { data: 'assign_by'},
                        { data: 'action'}
                    ],
                order: [
                    [0,'ASC']
                ],
                "columnDefs": [ 
                    {
                        "targets": [ 0 ],
                        "visible": false,
                        "searchable": false
                    },
                    {
                        "targets": [ 9 ],
                        "visible": url == 'assignedtask' || url == 'teamtask' ? true : false,
                        "searchable": false
                    },
                    {
                        "targets": [ 10 ],
                        "visible": url == 'mytask' || url == 'teamtask' || url == 'completedtask' || url == 'missedtask' ? true : false,
                        "searchable": false
                    },
                    {
                        "targets": [ 11 ],
                        "searchable": false,
                        "orderable": false
                    }
                ]
            
    });

}



$(document).on('click','.leadtask',function(){
    var url = $(this).attr('href');
    var pageUrl = 'userdetails';
    var tableid = '';
    var tab = 'daily';
    if(url == '#leadtask' || url == '#daily_leadtask'){
        tab = 'daily';
        tableid = '#daily_leadtask';
        $('#daily_leadtask-list').DataTable().destroy();
    }
    else if(url == '#weekly_leadtask'){
        tab = 'weekly';
        tableid = '#weekly_leadtask';
        $('#weekly_leadtask-list').DataTable().destroy();
    }else if(url == '#monthly_leadtask'){
        tab = 'monthly';
        tableid = '#monthly_leadtask';
        $('#monthly_leadtask-list').DataTable().destroy();
    }else if(url == '#allpast_accounttask'){
        tab = 'past';
        tableid = '#allpast_accounttask';
        $('#allpast_accounttask-list').DataTable().destroy();
    }
    fetch_lead_task_data(pageUrl,tableid,profile_user_id,tab);
})


function fetch_lead_task_data(pageUrl,tableid,profile_user_id,tab){
    var task_type = '';
    var dataTable =  $(tableid+'-list').DataTable({
        processing: true,
        serverSide: true,
        autoWidth:true,
        language: {
            sProcessing: "<div id='tableloader'><div id='inner_processing'><img src='{{asset('assets/images/loader4.gif')}}'></div></div>"
        },
        drawCallback: function() {
            tooltip()
        }, 
        ajax:{
            "url" : '{{route("fetch-leadfollowuplist")}}',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ BEARER
            },
            "type": "POST",
            "data" : { 
                        type : task_type,pageUrl,profile_user_id,tab
            }
        },
        columns: [
            { data: 'hide_followup_date'},
            { data: 'priority'},
            { data: 'task_date'},
            //{ data: 'subject'},
            { data: 'lead_name'},
            { data: 'lead_contact'},
            { data: 'description'},
            { data: 'followup_type'},
            //{ data: 'lead_address'},
            { data: 'assign_to'},
            { data: 'assign_by'},
            { data: 'status'},
            { data: 'action'}
        ],
        order: [
            [0,'DESC']
        ],
        "columnDefs": [ 
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                orderable: false, targets: 10
            }
        ]
    });
}
</script>
@include('account_task.create-account-task')
