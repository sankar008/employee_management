<script>

    let rd = (typeof accountRenderDiv === 'undefined') ? 'gridContainer' : accountRenderDiv;
    let exportIcon = document.getElementById('exportId');
    if(exportIcon != null && exportIcon != undefined) {
        function addExptBtn(){
            let tool_bar = document.getElementById(rd).querySelectorAll('.dx-toolbar-items-container')[0];
            if(typeof tool_bar !== "undefined") {
                tool_bar.appendChild(exportIcon);
                tool_bar.style.textAlign = 'right';
                exportIcon.style.marginRight = '45px';
            }
        }
        addExptBtn()
    }
</script>
