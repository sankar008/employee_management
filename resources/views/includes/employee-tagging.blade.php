
@php
$primary_so = array_filter($details->employeetagging->toArray(), function($item) {
    return $item['secondary_so_status'] == 0;
});
$secondary_so = array_filter($details->employeetagging->toArray(), function($item) {
    return $item['secondary_so_status'] == 1;
});
@endphp
<div class="account-form-title col-md-12 d-flex justify-content-between align-items-center">
  <h6> Employee Tagging Details</h6>
</div>
<form class="form-horizontal update-account-form employee_tagging_form" id="employee_tagging_form" action="javascript:void(0)">  
    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> AGM/RSM </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select AGM/RSM Employee" id="agm_emp" name="agm_emp">
          <option value=""></option>
          @foreach($AGM_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($details->employeetagging->toArray(), 'AGM'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    
    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> ASM </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select ASM Employee" id="asm_emp" name="asm_emp">
          <option value=""></option>
          @foreach($ASM_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($details->employeetagging->toArray(), 'ASM'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    

    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> Primary SO </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select Primary SO Employee" id="pri_so_emp" name="pri_so_emp">
          <option value=""></option>
          @foreach($SO_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($primary_so, 'SO'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> ME  </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select ME Employee" id="me_emp" name="me_emp">
        <option value=""></option>
          @foreach($ME_info as $resp_emp)
            <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($details->employeetagging->toArray(), 'ME'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> DTO (for WB)  </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select DTO Employee" id="dto_emp" name="dto_emp">
        <option value=""></option>
          @foreach($DTO_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($details->employeetagging->toArray(), 'DTO'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> DSO (for WB)  </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select DSO Employee" id="dso_emp" name="dso_emp">
        <option value=""></option>
          @foreach($DSO_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($details->employeetagging->toArray(), 'DSO'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>

    

    <div class="form-group row">
      <label for="inputName2" class="col-sm-4 col-form-label"> Secondary SO </label>
      <div class="col-sm-8">
        <select class="form-control select2bs4" data-placeholder="Select Secondary SO Employee" id="second_so_emp" name="second_so_emp[]" multiple>
        <option value=""></option>
          @foreach($SO_info as $resp_emp)
              <option value="{{$resp_emp->get_user_details->mob_id}}" @if(in_array($resp_emp->get_user_details->mob_id, array_column($secondary_so, 'SO'))) selected @endif>{{$resp_emp->get_user_details->name}}</option>
          @endforeach
        </select>
      </div>
    </div>
    <div class="blue-btn-wrap text-right">
            <input type="submit" class="btn btn-primary data-update" id="employee_tagging" data-type="employee_tagging" value="Update">
        </div>
    </form>