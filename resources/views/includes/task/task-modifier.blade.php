<div class="modal fade show" id="remark-task-modal" aria-modal="true" style="padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Task Remark <span class="account_name badge badge-success"></span> <span class="account_type badge badge-info"></span></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body" style="padding-top:0">
                <form action="javascript:void(0)" method="post" id="task-update-form">
                    <div class="row">
                        <div class="card-body col-md-12 card-body2">
                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Task status <span class="required">*</span> </label>
                                <select class="form-control select2bs4" data-placeholder="Select task status" id="task_status" name="task_status">
                                    <option value=""></option>
                                    {{-- <option value="0">Open</option> --}}
                                    <option value="1">Completed</option>
                                    <option value="2">Reschedule</option>
                                </select>
                                <div class="row custom_radio_class"><label for="task_status" class="error" style="display:none;"></label></div>
                            </div>

                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Task due date</label>
                                <input type="text" name="task_due_date" class="form-control basic_datepicker" value="" readonly disabled>
                                <div class="row custom_radio_class"><label for="account_id" class="error" style="display:none;"></label></div>
                            </div>


                            {{-- <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Account Type</label>
                                <select class="form-control select2bs4" data-placeholder="Select account type" id="account_type" name="account_type" disabled>
                                    <option value=""></option>
                                    <option value="18">Distributor</option>
                                    <option value="39">Dealer</option>
                                    <option value="44">Engineer</option>
                                    <option value="45">Mason</option>
                                    <option value="46">Petty Contractor</option>
                                    <option value="40">Sub Dealer</option>
                                </select>
                                <div class="row custom_radio_class"><label for="account_type" class="error" style="display:none;"></label></div>
                            </div>

                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Account List</label>
                                <select class="form-control select2bs4" data-placeholder="Select account" id="account_id" name="account_id" disabled>
                                    <option value=""></option>
                                </select>
                                <div class="row custom_radio_class"><label for="account_id" class="error" style="display:none;"></label></div>
                            </div>

                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Assign Employee</label>
                                <select class="form-control select2bs4" data-placeholder="Select Responsible employee" id="responsible_user" name="responsible_user" disabled>
                                    <option value=""></option>
                                    @if(!$employees->isEmpty())
                                        @foreach($employees as $eachEmployee)
                                            <option value="{{$eachEmployee->id}}">{{$eachEmployee->name}}</option>
                                        @endforeach
                                    @endif
                                </select>
                                <div class="row custom_radio_class"><label for="responsible_user" class="error" style="display:none;"></label></div>
                            </div> --}}

                            {{-- <div class="form-group inner-addon right-addon">
                                <div class="form-group inner-addon right-addon pt-2">
                                    <label for="exampleInputEmail1">Self task</label>
                                    <div class="icheck-primary d-inline ml-2">
                                        <input class="mclass2 cclass38 cclass" type="checkbox" name="self_task" id="self_task" value="1">
                                        <label for="self_task">Yes</label>
                                    </div>
                                </div>
                                <div class="row custom_radio_class"><label for="self_task" class="error" style="display:none;"></label></div>
                            </div> --}}
                        </div>

                        <div class="card-body col-md-4 card-body2" style="display: none">
                            {{-- <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Task priority <span class="required_star">*</span></label>
                                <select class="form-control select2bs4" data-placeholder="Select task priority list" id="task_priority" name="task_priority">
                                    <option></option>
                                    @if(!$priorities->isEmpty())
                                    @foreach($priorities as $eachPriority)
                                        <option value="{{$eachPriority->priority_id}}">{{$eachPriority->priority_name}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="row custom_radio_class"><label for="task_priority" class="error" style="display:none;"></label></div>
                            </div>

                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Task type <span class="required_star">*</span></label>
                                <select class="form-control select2bs4" data-placeholder="Select task type list" id="task_type" name="task_type">
                                    <option></option>
                                    @if(!$events->isEmpty())
                                    @foreach($events as $eachEvent)
                                        <option value="{{$eachEvent->event_id}}">{{ucwords($eachEvent->event_name)}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="row custom_radio_class"><label for="task_type" class="error" style="display:none;"></label></div>
                            </div> --}}

                            {{-- <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1">Task Purpose</label>
                                <select class="form-control select2bs4" data-placeholder="Select task purpose list" id="task_purpose" name="task_purpose">
                                    <option></option>
                                    @if(!$purposes->isEmpty())
                                    @foreach($purposes as $eachPurpose)
                                        <option value="{{$eachPurpose->purpose_id}}">{{ucwords($eachPurpose->purpose_name)}}</option>
                                    @endforeach
                                    @endif
                                </select>
                                <div class="row custom_radio_class"><label for="task_purpose" class="error" style="display:none;"></label></div>
                            </div> --}}

                            {{-- <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1"> Task Date <span class="required_star">*</span></label>
                                <input type="text" class="datepicker form-control basic_datepicker" id="task_date" name="task_date" readonly>
                            </div> --}}
                        </div>

                        <div class="card-body col-md-12 card-body2">
                            <div class="form-group inner-addon right-addon">
                                <label for="exampleInputEmail1"> Remarks <span class="required_star">*</span></label>
                                <textarea type="text" class="form-control" placeholder="Task Description" id="task_description" name="task_description"></textarea>
                            </div>
                        </div>

                    </div>

                    <div class="row" style="float:right;" id="add_task_save">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <input type="submit" class="btn btn-primary update_task" value="Update" data-pid="1">
                            {{-- <input type="submit" class="btn btn-success save_task" value="Save & Add New" data-pid="2">
                            <input type="reset" class="btn btn-info form_reset" value="Reset"> --}}
                            <button type="reset" class="btn btn-danger form_reset" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                    {{-- <div class="row" style="float:right;display:none;" id="update_task_save">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <input type="submit" class="btn btn-success" id="Update" value="Update" data-pid="3">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                        </div>
                    </div> --}}

                    <input type="hidden" name="task_id" id="task_id" value="0">
                    <input type="hidden" name="task_parent_id" id="task_parent_id" value="0">
                    <input type="hidden" name="task_reschedule_count" id="task_reschedule_count" value="0">
                    <input type="hidden" name="is_account_task" id="is_account_task" value="0">
                </form>
            </div>
        </div>
    </div>
</div>
