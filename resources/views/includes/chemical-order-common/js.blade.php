<!-- BDC Dispatch Details -->
<div class="modal fade" id="dispatch-details-modal">
    <div class="modal-dialog modal-lg" style="max-width: 1000px !important;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Dispatch Details</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×
                </button>
            </div>
            <div class="modal-body" id="dispatch-data">

            </div>
        </div>
    </div>
</div>
@section('script')
<script type="text/javascript">
    $(document).on('click', '.show_dispatch_status_btn', function(){
    var bdcs = $(this).data('bdc');
    console.log(bdcs)
    $("#dispatch-details-modal").modal('show')
    var html = `<h5>BDC Report</h5>
    <table class='table table-responsive'>
        <thead>
            <tr>
                <th>CRM Order ID</th>
                <th>Invoice No</th>
                <th>Invoice Date</th>
                <th>Quantity Dispatched</th>
                <th>Material SAP</th>
                <th>Material Description</th>
                <th>Ship To Name</th>
                <th>Ship To SAP</th>
            </tr>
        </thead>
        <tbody>
    `;

    $.each(bdcs, (i) => {
        html += `<tr>
                <td>${bdcs[i].crm_order_id}</td>
                <td>${bdcs[i].invoice_no}</td>
                <td>${bdcs[i].invoice_date}</td>
                <td>${bdcs[i].quantity}</td>
                <td>${bdcs[i].material}</td>
                <td>${bdcs[i].material_description}</td>
                <td>${bdcs[i].ship_to_name}</td>
                <td>${bdcs[i].ship_to_sap}</td>
            </tr>`
    })

    html += `</tbody></table>`;

    $("#dispatch-data").html(html)

})

</script>
@stop
