    <script>
        const module_id = '{{Request::route('module_id')}}'
        $(document).ready(function () {
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            });
        })

       
    @if(Route::currentRouteName() == 'manage.module')
    fetch_menus();
        function fetch_menus(){
            $('#menu-list').DataTable().destroy()
            var dataTable =  $('#menu-list').DataTable({
                processing: true,
                serverSide: true,
                stateSave: true,
                pageLength:30,
                ajax: '{{route("manage.module.fetch")}}',
                language: {
                    sProcessing: "<div id='tableloader'><div id='inner_processing'><img src='{{asset('assets/images/loader4.gif')}}'></div></div>",
                    searchPlaceholder: "Search by name or employee code"
                },
                columns: [
                    {data: 'menu_name', name:'menu_name'},
                    { data: 'action'}
                ]
            })
        }
    @endif

        /* click event */
        $('.addEditModule').on('click',function(e){
            e.preventDefault()
            var title = $(this).attr('data-title')
            $('.modal-title').html(title)
        })

        /* validation */
        $('#add-edit-module-form').validate({
            rules: {
                menu_name: {
                    required : true,
                    //pattern: /^[^-\s][a-zA-Z\s-]+$/,
                    maxlength: 40,
                    //minlength: 5
                },
                menu_slug: {
                    required : false,
                },
                menu_icon: {
                    required : false
                }
            },
            messages: {
                menu_name: {
                    required : 'Please enter module name',
                    pattern:   "Module name can\'t contain any special charecter or number",
                    maxlength: "Module name can\'t longer than 40 charecters",
                    minlength: "Module name can\'t lesser than 5 charecters",
                }
            },
            submitHandler: function(){
                var module_id = $('#moduleID').val()
                var menu_id   = $('#menuID').val()
                var module_type = $('#moduleType').val()
                var moduleCharType = $('#moduleCharType').val()
                var menuParentID   = $('#menuParentID').val()
                var url = '{{route("module.addEdit", ":module_id")}}'
                @if(Route::currentRouteName() == 'manage.module.parent')
                   var url = '{{route("module.addEdit.child", ":module_id")}}'
                @endif
                
                url = url.replace(":module_id",module_id)
                var data = $('#add-edit-module-form').serializeArray()
                dataObj = {}
                buttons = []
                tabs = []
                $.each(data, function(i,field){
                    dataObj[field.name] = field.value
                    if(field.name == 'tab_list'){
                        tabs.push(field.value);
                    }

                    if(field.name == 'button_list'){
                        buttons.push(field.value);
                    }
                })

                dataObj = {
                    'data': dataObj,
                    'tabs': tabs,
                    'buttons': buttons,
                    'module_id': module_id,
                    'module_type': module_type,
                    'moduleCharType': moduleCharType,
                    'menu_id': menu_id,
                    'parent_id': menuParentID
                }

                var data = JSON.stringify(dataObj)
                $.ajax({
                    url: url,
                    type: 'POST',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: {data},
                    complete: function(payload){
                        var status = payload.status;
                        if(status == 201 || status == 200){
                            toastr.success('New menu item added successfully')
                            $('#add_edit_module_modal').modal('hide')
                            $("#add-edit-module-form").trigger("reset")

                            @if(Route::currentRouteName() == 'manage.module.parent') 
                            $('.dd').nestable('destroy') 
                                loadMenus(module_id)
                            @elseif(Route::currentRouteName() == 'manage.module') 
                                fetch_menus() 
                            @endif
                        }else{
                            toastr.error('Something went wrong, please try again later')
                        }
                    }
                });
            }
        })


        /* open modal with data render */
        $('.addEditModule').on('click', function(){
            $('#moduleCharType').val('new')
            $.ajax({
                url: '{{route('modules.fetch')}}',
                type: 'GET',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {},
                complete: function(payload){
                    var response = payload.responseJSON
                    var html = ''
                    $('#button_list').find('option').not(':first').remove()
                    $.each(response.buttons,function(i){
                        var html = '<option>' + response.buttons[i].name + '</option>'
                        $('#button_list').append(html)
                    })
                    var html = ''
                    $('#tab_list').find('option').not(':first').remove()
                    $.each(response.tabs,function(i){
                        var html = '<option>' + response.tabs[i].name + '</option>'
                        $('#tab_list').append(html)
                    })
                }
            })

            @if(Route::currentRouteName() == 'manage.module.parent')
            loadParentItems()
            @endif
        })

        function loadParentItems (parent_id = '') {
            let url = '{{route("manage.module.fetchparent", ":module_id")}}'
            url = url.replace(":module_id",module_id)
            $.ajax({
                url: url,
                type: 'POST',
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                data: {module_id},
                complete: function(payload){
                    var response = payload.responseJSON
                    var html = ''
                    $('#parent_menu').find('option').not(':first').remove()
                    $.each(response.data,function(i){
                        var html = '<option value="'+response.data[i].id+'">' + response.data[i].menu_name + '</option>'
                        $('#parent_menu').append(html)
                        if(parent_id != '' && parent_id > 0) {
                            $('#parent_menu').val(parent_id).trigger('change')
                        }
                    })
                }
            })
        }
    </script>
