<!-- import modal -->
<div class="modal fade" id="import-engineers-redemption">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="overlay d-flex justify-content-center align-items-center loader">
                <img src="{{ asset('assets/images/spinner.gif') }}" alt="">
            </div>
            <div class="modal-header">
                <h4 class="modal-title">Import Bulk Redemption Points</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" method="post" id="redemption-form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-12 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Select File <span class="required_star">*</span></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label for="exampleInputFile">File input <span class="required">(.csv files are
                                            accepted)</span></label>
                                    <input type="file" class="custom-file-input" id="file-redem" name="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="float:right;">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <a href="{{ asset('samples/redemption-import.csv') }}" class="btn btn-success"
                                target="_blank" download="redemption-import.csv">Download Sample</a>
                            <button type="submit" class="btn btn-primary" id="redemption-btn">Import</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>