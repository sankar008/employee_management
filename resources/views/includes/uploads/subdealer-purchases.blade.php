<!-- import modal -->
<div class="modal fade" id="import-sdpurchase-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="overlay d-flex justify-content-center align-items-center loader">
                <img src="{{ asset('assets/images/spinner.gif') }}" alt="">
            </div>
            <div class="modal-header">
                <h4 class="modal-title">Import Bulk Sub Dealer Purchases</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" method="post" id="import-lead-form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6 form-group inner-addon right-addon d-none">
                            <label for="exampleInputFile">Year <span class="required_star">*</span></label>
                            <div class="input-group">
                                @php($years=array(date('Y')-1,date('Y'),date('Y')+1))
                                    <select name="year" id="year1" class="form-control">
                                        <option value='2020'>2020</option>
                                        @foreach($years as $year)
                                            <option>{{ $year }}</option>
                                        @endforeach
                                    </select>
                         
                            </div>
                        </div>
                        <div class="col-lg-6 form-group inner-addon right-addon d-none">
                            <label for="exampleInputFile">Month <span class="required_star">*</span></label>
                            <div class="input-group">
                                @php($months=array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Aprl'=>'04','May'=>'05','June'=>'06','July'=>'07','Aug'=>'08','Sept'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12'))
                                    <select name="month" id="month1" class="form-control">
                                        <option value="12">Dec</option>
                                        <!-- <option value=''>--SELECT MONTH--</option> -->
                                        @foreach($months as $month => $num)
                                            <option value={{ $num }}>{{ $month }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-lg-4 form-group inner-addon right-addon d-none">
                            <label for="exampleInputFile">Unit <span class="required_star">*</span><br></label>
                            <div class="row custom_radio_class">
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender6" value="kg" name="unit1">
                                    <label for="radioGender6">Kg</label>
                                </div>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender7" value="mt" name="unit1" checked>
                                    <label for="radioGender7">MT</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Select File <span class="required_star">*</span></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label for="exampleInputFile">File input <span class="required">(.csv files are
                                            accepted)</span></label>
                                    <input type="file" class="custom-file-input" id="file1" name="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
  
                            </div>
                        </div>
                    </div>
                    <div class="row" style="float:right;">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <a href="{{ asset('samples/sdp-import.csv') }}" class="btn btn-success"
                                target="_blank" download="sdp-import.csv">Download Sample</a>
                            <button type="submit" class="btn btn-primary" id="import_sdp">Import</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>