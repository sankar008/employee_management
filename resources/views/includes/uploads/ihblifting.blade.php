<!-- import modal -->
<div class="modal fade" id="import-ihb-modal">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="overlay d-flex justify-content-center align-items-center loader">
                <img src="{{ asset('assets/images/spinner.gif') }}" alt="">
            </div>
            <div class="modal-header">
                <h4 class="modal-title">Import Bulk IHB Lifitings</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="javascript:void(0)" method="post" id="import-lead-form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-lg-6 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Year <span class="required_star">*</span></label>
                            <div class="input-group">
                                @php($years=array(date('Y')-1,date('Y'),date('Y')+1))
                                    <select name="year" id="year" class="form-control">
                                        <option value=''>--SELECT YEAR--</option>
                                        @foreach($years as $year)
                                            <option>{{ $year }}</option>
                                        @endforeach
                                    </select>

                            </div>
                        </div>
                        <div class="col-lg-6 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Month <span class="required_star">*</span></label>
                            <div class="input-group">
                                @php($months=array('Jan'=>'01','Feb'=>'02','Mar'=>'03','Aprl'=>'04','May'=>'05','June'=>'06','July'=>'07','Aug'=>'08','Sept'=>'09','Oct'=>'10','Nov'=>'11','Dec'=>'12'))
                                    <select name="month" id="month" class="form-control">
                                        <option value=''>--SELECT MONTH--</option>
                                        @foreach($months as $month => $num)
                                            <option value={{ $num }}>{{ $month }}</option>
                                        @endforeach
                                    </select>
                            </div>
                        </div>
                        <div class="col-lg-6 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Type of Project <span
                                    class="required_star">*</span><br></label>
                            <div class="row custom_radio_class">
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender1" value="2" name="ptype">
                                    <label for="radioGender1">Govt</label>
                                </div>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender2" value="1" name="ptype">
                                    <label for="radioGender2">Private</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Unit <span class="required_star">*</span><br></label>
                            <div class="row custom_radio_class">
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender3" value="kg" name="unit">
                                    <label for="radioGender3">Kg</label>
                                </div>
                                <div class="icheck-primary d-inline">
                                    <input type="radio" id="radioGender4" value="mt" name="unit" checked>
                                    <label for="radioGender4">MT</label>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 form-group inner-addon right-addon">
                            <label for="exampleInputFile">Select File <span class="required_star">*</span></label>
                            <div class="input-group">
                                <div class="custom-file">
                                    <label for="exampleInputFile">File input <span class="required">(.csv files are
                                            accepted)</span></label>
                                    <input type="file" class="custom-file-input" id="file" name="file">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="row" style="float:right;">
                        <div class="col-4" style="flex:none;max-width:100%">
                            <a href="{{ asset('samples/ihb-import.csv') }}" class="btn btn-success"
                                target="_blank" download="ihb-import.csv">Download Sample</a>
                            <button type="submit" class="btn btn-primary" id="import_ihb">Import</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
