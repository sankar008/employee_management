
    <nav class="main-header navbar navbar-expand navbar-dark navbar-primary">
        <!-- Left navbar links -->
        <ul class="navbar-nav">
        <li class="nav-item mobile-visible" style="display: none">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{route('document.dashboard')}}" class="nav-link">Dashboard</a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="#" class="nav-link">Contact</a>
        </li> -->
        </ul>

        <!-- SEARCH FORM -->
        <!-- <form class="form-inline ml-3">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
            <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
            </button>
            </div>
        </div>
        </form> -->

        <!-- Right navbar links -->
        <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" href="#">
            <i class="fas fa-wifi green" style="display:none" id="online" data-toggle="tooltip" data-placement="left" title="Online"></i>
            <i class="fas fa-wifi red" style="display:none" id="offline" data-toggle="tooltip" data-placement="left" title="No internet"></i>
            </a>
        </li>

        <li class="nav-item custom-navlink">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="settings" href="#">
                <i class="fas fa-cogs"></i>
            </a>
        </li>

        <li class="nav-item custom-navlink">
            <a class="nav-link" data-toggle="tooltip" data-placement="right" title="Logout" href="#">
                <i class="fas fa-sign-out-alt"></i>
            </a>
        </li>
        </ul>
    </nav>
