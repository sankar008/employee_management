<!doctype html>
<html>
<head>
    @include('includes.login_header')
</head>
    <body class="hold-transition login-page-v3">
    <!-- login-box -->
    @yield('content')
    <!-- /.login-box -->
    @include('includes.login_footer')
    @yield('script')
    </body>
</html>
