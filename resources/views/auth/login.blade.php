@extends('layouts.app')
@section('content')
    <div class="login-overlay"></div>
    <div class="login-box">
        <div class="login-logo">
            <a href="{{config('app.parent_url')}}"><img src="{{asset('assets/images/shyam_logo_img.png')}}"></a>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to CRM</p>
                <form action="{{ route('login') }}" method="post" >
                    @csrf
                    <div class="input-group mb-3">
                        <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-user"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row custom_radio_class"><label for="username" class="error" style="display:none;"></label></div>
                    <div class="input-group mb-3">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fa fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row custom_radio_class"><label for="password" class="error" style="display:none;"></label></div>
                    <div class="row">
                        <div class="col-7">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember" name="remember" value="1" @if (Cookie::get('username') !== null)  checked @endif>
                                <label for="remember">
                                Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-5">
                            <button type="submit" class="btn btn-primary btn-block btn-flat" id="sign_in">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <p class="mb-1">
                    <a href="{{route('password.request')}}">Forgot my password</a>
                </p>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
@endsection
