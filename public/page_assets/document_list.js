const getDocument = () => {

    const provider = new DevExpress.fileManagement.CustomFileSystemProvider({
        getItems,
        // createDirectory,
        // renameItem,
        // deleteItem,
        // copyItem,
        // moveItem,
        // uploadFileChunk,
        // downloadItems,
      });

    $("#file-manager").dxFileManager({
        name: "fileManager",
        fileSystemProvider: provider,
        height: 450,
        onDirectoryCreated: function(e) {
            //console.log(e.parentDirectory.name);

        },
        uploadFileChunk: function(e){
            console.log(e);
        },
        onFileCreated: function(e){
            console.log(e);
        },
        permissions: {
            create: true,
            copy: false,
            move: false,
            delete: true,
            rename: true,
            upload: true,
            download: true,
        },
    });

    function getItems(parentDirectory) {

        


        return [
                    {
                      name: 'About.xml',
                      isDirectory: false,
                      size: 1024,
                    },
                    {
                      name: 'Managers.rtf',
                      isDirectory: false,
                      size: 2048,
                    },
                    {
                      name: 'ToDo.txt',
                      isDirectory: false,
                      size: 3072,
                    },
        ]
    }
};

getDocument();
