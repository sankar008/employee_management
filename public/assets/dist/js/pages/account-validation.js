

    $(document).on('click', '.addressremove', function(event) {
        var position_id = $(this).attr('id');
        var info = position_id.split('-');
        $("#address-level-"+info[1]).remove();
        if(info[2] > 0)
        {
            var already = $("#removeid").val();
            if(already != '')
            {
                $("#removeid").val(already+','+info[2]);
            }
            else{
                $("#removeid").val(info[2]);
            }
        }
    });

    /*
    * get district list
    */
    // $(document).on('change','.stateclass',function(event){
    //     var position_id = $(this).attr('id');
    //     var position_info = position_id.split('-');
    //     var state_id = $("#"+position_id).val();
    //     var type     = 0;
    //     $("#district-"+position_info[1]).html('');
    //     $("#route-"+position_info[1]).html('');
    //     $.ajax({
    //         url: districturl,
    //         type: 'GET',
    //         headers: {
    //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
    //             'Authorization': 'Bearer '+ BEARER
    //             },
    //         data: {state_id,type},
    //         complete: function(payload){
    //             var newData = payload.responseJSON;
    //             var content = '<option></option>';
    //             $.each(newData.data, function(i, item) {
    //                 content =content+' <optgroup label="'+i+'">';
    //                 $.each(item, function(j, value) {
    //                     content =content+ '<option value="'+value.id+'">'+value.name+'</option>';
    //                 });
    //                 content = content +'</optgroup>';
    //             });
    //             $("#district-"+position_info[1]).html(content);
    //             //$("#district-"+position_info[1]).val([]).trigger('change');
    //         }
    //     });
    // });

 


    /*
    * get block list
    */
    $(document).on('change','.districtclass',function(event){
        var position_id     = $(this).attr('id');
        var position_info   = position_id.split('-');
        var state_id        = $("#state-"+position_info[1]).val();
        var district_id     = $("#"+position_id).val();
        var type            = 2;
        $("#block_id-"+position_info[1]).html('');
        $("#route-"+position_info[1]).html('');
        $.ajax({
            url: blockurl,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ BEARER
                },
            data: {state_id,district_id,type},
            complete: function(payload){
                var newData = payload.responseJSON;
                var content = '<option></option>';
                $.each(newData.data, function(i, item) {
                    content =content+' <optgroup label="'+i+'">';
                    $.each(item, function(j, value) {
                        content =content+ '<option value="'+value.block_id+'">'+value.block_name+'</option>';
                    });
                    content = content +'</optgroup>';
                });
                $("#block_id-"+position_info[1]).html(content);
                $("#block_id-"+position_info[1]).val([]).trigger('change');
            }
        });
            
    });

    $(document).on('change','.addresstypeclass',function(){
        var position_id         = $(this).attr('id');
        var position_info       = position_id.split('-');
        var address_type_val    = $("#"+position_id).val();
        var content = '';
        if(address_type_val == 1){
            content = ' <div class="icheck-primary d-inline"><input type="radio" id="home-'+position_info[1]+'" value="'+position_info[1]+'" name="home"><label for="home-'+position_info[1]+'">Yes</label></div>';
        }
        if(address_type_val == 2){
            content = ' <div class="icheck-primary d-inline"><input type="radio" id="office-'+position_info[1]+'" value="'+position_info[1]+'" name="office"><label for="office-'+position_info[1]+'">Yes</label></div>';
        }
        if(address_type_val == 4){
            content = ' <div class="icheck-primary d-inline"><input type="radio" id="godown-'+position_info[1]+'" value="'+position_info[1]+'" name="godown"><label for="godown-'+position_info[1]+'">Yes</label></div>';
        }
        if(address_type_val == 3){
            content = ' <div class="icheck-primary d-inline"><input type="radio" id="counter-'+position_info[1]+'" value="'+position_info[1]+'" name="counter"><label for="counter-'+position_info[1]+'">Yes</label></div>';
        }
        $("#isdefault-show-"+position_info[1]).show();
        $("#isdefault-div-"+position_info[1]).html(content);

    });


    $(document).on('change','.blocklistclass',function(event){
        var position_id     = $(this).attr('id');
        var position_info   = position_id.split('-');
        var state_id        = $("#state-"+position_info[1]).val();
        var district_id     = $("#district-"+position_info[1]).val();
        var block_id        = $("#"+position_id).val();
        var type            = 2;
        $("#route-"+position_info[1]).html('');
        // var content = '<option value="0">Other</option>';
        // $("#route-"+position_info[1]).html(content);
        // $("#route-"+position_info[1]).val([]).trigger('change');
        $.ajax({
            url: routeurl,
            type: 'GET',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'), 
                'Authorization': 'Bearer '+ BEARER
                },
            data: {state_id,district_id,block_id},
            complete: function(payload){
                var newData = payload.responseJSON;
                console.log(newData);
                var content = '<option></option>';
                $.each(newData.data, function(i, item) {
                    content =content+ '<option value="'+item.id+'">'+item.route_name+'</option>';
                });
                content =content+ '<option value="0">Other</option>';
                $("#route-"+position_info[1]).html(content);
                $("#route-"+position_info[1]).val([]).trigger('change');
            }
        });  
    });
    
    $(document).on('change','.routeclass',function(event){
        var position_id     = $(this).attr('id');
        var position_info   = position_id.split('-');
        var route_id        = $("#"+position_id).val();
        $("#routenameid-"+position_info[1]).hide();
        if(route_id == 0)
        {
            $("#routenameid-"+position_info[1]).show();
        }
    });
 
  